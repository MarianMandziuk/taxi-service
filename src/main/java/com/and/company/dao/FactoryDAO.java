package com.and.company.dao;

import com.and.company.dao.implementation.*;

public class FactoryDAO {

    private static FactoryDAO factoryDAO;

    public AccountDAO createAccountDAO() {
        return new AccountJDBC();
    }

    public CarDAO createCarDAO() {
        return new CarJDBC();
    }

    public OrderDAO createOrderDAO() {
        return new OrderJDBC();
    }

    public CategoryDAO createCategoryDAO() {
        return new CategoryJDBC();
    }

    public LoyaltyDAO createLoyaltyDAO() {
        return new LoyaltyJDBC();
    }

    public LanguageDAO createLanguageDAO() {
        return new LanguageJDBC();
    }

    public CarParkAddressDAO createParkAddressDAO() {
        return new CarParkAddressJDBC();
    }

    public static FactoryDAO getInstance() {
        if (factoryDAO == null) {
            synchronized (FactoryDAO.class) {
                if (factoryDAO == null) {
                    factoryDAO = new FactoryDAO();
                }
            }
        }
        return factoryDAO;
    }
}