package com.and.company.dao;

import com.and.company.entity.Language;

public interface LanguageDAO {
    Language getLanguage(String langName);
}
