package com.and.company.dao.implementation;

import com.and.company.dao.CarDAO;
import com.and.company.db.DBManager;
import com.and.company.entity.Car;
import com.and.company.entity.Category;
import com.and.company.entity.Status;
import org.apache.log4j.Logger;

import java.util.List;

import java.sql.*;
import java.util.ArrayList;

public class CarJDBC implements CarDAO {
    private final Logger LOGGER = Logger.getLogger(CarJDBC.class);

    private static final String CAR_BY_CAPACITY_AND_CATEGORY =
        "SELECT car.*, category_translation.id AS category_translation_id, \n" +
        "category_translation.description AS category, category_translation.cost_by_km,\n" +
        "category_translation.language_id,\n" +
        "status.description AS status_description\n" +
        "FROM car \n" +
        "INNER JOIN category\n" +
        "ON car.id = category.car_id\n" +
        "INNER JOIN category_translation\n" +
        "ON category_translation.id = category.category_translation_id\n" +
        "INNER JOIN status\n" +
        "ON car.status_id = status.id\n" +
        "INNER JOIN language\n" +
        "ON category_translation.language_id = language.id\n" +
        "WHERE language.id = ?\n" +
        "AND car.capacity >= ?\n" +
        "AND status.description = ?\n" +
        "AND category_translation.description = ?\n" +
        "LIMIT 1;";

    private static final String CARS_BY_CAPACITY =
        "select car1.*\n" +
        "from car car1\n" +
        "left join car car2 on car1.id >= car2.id\n" +
        "\n" +
        "INNER JOIN category\n" +
        "ON car1.id = category.car_id\n" +
        "INNER JOIN category_translation\n" +
        "ON category_translation.id = category.category_translation_id\n" +
        "INNER JOIN status\n" +
        "ON car1.status_id = status.id\n" +
        "WHERE category_translation.language_id = ?\n" +
        "AND status.description = ?\n" +
        "AND category_translation.description = ? \n" +
        "\n" +
        "group by car1.id\n" +
        "having sum(car2.capacity) <= (select sum(car2.capacity) as capa\n" +
            "from car car1\n" +
            "left join car car2 on car1.id >= car2.id\n" +
            "INNER JOIN category\n" +
            "ON car1.id = category.car_id\n" +
            "INNER JOIN category_translation\n" +
            "ON category_translation.id = category.category_translation_id\n" +
            "INNER JOIN status\n" +
            "ON car1.status_id = status.id\n" +
            "WHERE category_translation.language_id = ?\n" +
            "AND status.description = ?\n" +
            "AND category_translation.description = ? \n" +
            "                \n" +
            "group by car1.id\n" +
            "having capa >= ?\n" +
            "limit 1);";

    private static final String UPDATE_CAR_STATUS =
            "UPDATE car SET car.status_id = ? WHERE car.id = ?;";

    private DBManager dbManager = DBManager.getInstance();

    public Car getAvailableCarByCapacityAndStatus(Long languageId,
            Integer capacity, Status status, String category) {
        Car car = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(CAR_BY_CAPACITY_AND_CATEGORY);
            int k = 1;
            ps.setLong(k++, languageId);
            ps.setInt(k++, capacity);
            ps.setString(k++, status.name());
            ps.setString(k++, category);

            rs = ps.executeQuery();
            if (rs.next()) {
                car = mapCar(rs);
            }
        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }

        return car;
    }

    @Override
    public List<Car> getSeveralCarsByCapacity(Long languageId, Integer capacity, Status status,
            String category) {
        List<Car> cars = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(CARS_BY_CAPACITY);
            int k = 1;
            ps.setLong(k++, languageId);
            ps.setString(k++, status.name());
            ps.setString(k++, category);
            ps.setLong(k++, languageId);
            ps.setString(k++, status.name());
            ps.setString(k++, category);
            ps.setInt(k++, capacity);

            rs = ps.executeQuery();
            while (rs.next()) {
                cars.add(mapCar(rs));
            }
        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }

        return cars;
    }

    public void updateCarStatus(Long carId, Status carStatus) {
        try (Connection con = dbManager.getConnection();
             PreparedStatement ps = con.prepareStatement(UPDATE_CAR_STATUS)) {
            int k = 1;
            ps.setLong(k++,carStatus.ordinal());
            ps.setLong(k, carId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.error(ex);
        }
    }
//
//    public List<Car> getAvailableCarsByCapacity(Integer capacity) {
//
//    }

    private Car mapCar(ResultSet rs) throws SQLException {
        Car car = new Car();
        car.setId(rs.getLong("id"));
        car.setModelName(rs.getString("model_name"));
        car.setCapacity(rs.getInt("capacity"));
        car.setStatusId(rs.getLong("status_id"));
        return car;
    }

    private Category mapCategory(ResultSet rs) throws SQLException {
        Category category = new Category();
        category.setId(rs.getLong("category_translation_id"));
        category.setDescription(rs.getString("category"));
        category.setCostPerKM(rs.getBigDecimal("cost_by_km"));
        category.setLanguageId(rs.getInt("language_id"));
        return category;
    }
}
