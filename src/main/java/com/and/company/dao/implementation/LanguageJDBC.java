package com.and.company.dao.implementation;

import com.and.company.dao.LanguageDAO;
import com.and.company.db.DBManager;
import com.and.company.entity.Car;
import com.and.company.entity.Language;
import org.apache.log4j.Logger;

import java.sql.*;

public class LanguageJDBC implements LanguageDAO {
    private final Logger LOGGER = Logger.getLogger(LanguageJDBC.class);

    private static final String GET_LAN_ENTITY_BY_NAME =
            "SELECT * FROM language WHERE name = ? LIMIT 1;";

    private DBManager dbManager = DBManager.getInstance();

    @Override
    public Language getLanguage(String langName) {
        Language language = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(GET_LAN_ENTITY_BY_NAME);
            ps.setString(1, langName);
            rs = ps.executeQuery();
            if (rs.next()) {
                language = new Language(
                        rs.getLong("id"),
                        rs.getString("name"));
            }
        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }

        return language;
    }
}
