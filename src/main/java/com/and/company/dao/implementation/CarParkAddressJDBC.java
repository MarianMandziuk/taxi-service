package com.and.company.dao.implementation;

import com.and.company.dao.CarParkAddressDAO;
import com.and.company.db.DBManager;
import com.and.company.entity.CarParkAddress;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CarParkAddressJDBC implements CarParkAddressDAO {
    private final Logger LOGGER = Logger.getLogger(CarParkAddressJDBC.class);

    private static final String UPDATE_UDRESS =
            "UPDATE car_park_address SET address = ?, longitude = ?, latitude = ? WHERE car_park_address.id = ?;";
    private static final String GET_PARK_CAR_ADDRESS =
            "SELECT * FROM car_park_address Limit 1";

    private DBManager dbManager = DBManager.getInstance();

    @Override
    public void updateAddress(CarParkAddress address) {
        try (Connection con = dbManager.getConnection();
             PreparedStatement ps = con.prepareStatement(UPDATE_UDRESS)) {
            int k = 1;
            ps.setString(k++, address.getAddress());
            ps.setBigDecimal(k++, address.getLongitude());
            ps.setBigDecimal(k++, address.getLatitude());
            ps.setLong(k, address.getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.error(ex);
        }
    }

    @Override
    public CarParkAddress getCarParkAddress() {
        CarParkAddress address = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(GET_PARK_CAR_ADDRESS);

            rs = ps.executeQuery();
            if (rs.next()) {
                address = new CarParkAddress();
                address.setId(rs.getLong("id"));
                address.setAddress(rs.getString("address"));
                address.setLongitude(rs.getBigDecimal("longitude"));
                address.setLatitude(rs.getBigDecimal("latitude"));
            }
        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }

        return address;
    }
}
