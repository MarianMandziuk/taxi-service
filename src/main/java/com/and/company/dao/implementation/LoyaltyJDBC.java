package com.and.company.dao.implementation;

import com.and.company.dao.LoyaltyDAO;
import com.and.company.dao.mapper.LoyaltyMapper;
import com.and.company.dao.mapper.Mapper;
import com.and.company.db.DBManager;
import com.and.company.entity.Loyalty;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoyaltyJDBC implements LoyaltyDAO {
    private final Logger LOGGER = Logger.getLogger(LoyaltyJDBC.class);

    private static final String GET_LOYALTY_BY_ID =
            "SELECT loyalty.*\n" +
            "FROM loyalty\n" +
            "WHERE loyalty.id = ?";

    private DBManager dbManager = DBManager.getInstance();

    @Override
    public Loyalty getLoyalty(Long id) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Loyalty loyalty = null;
        Mapper<Loyalty> loyaltyMapper = new LoyaltyMapper();
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(GET_LOYALTY_BY_ID);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                loyalty = loyaltyMapper.getEntity(rs);
            }
        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }
        return loyalty;
    }
}
