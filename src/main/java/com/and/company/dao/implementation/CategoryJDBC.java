package com.and.company.dao.implementation;

import com.and.company.bean.ClientOrderBean;
import com.and.company.dao.CategoryDAO;
import com.and.company.dao.mapper.CategoryMapper;
import com.and.company.dao.mapper.Mapper;
import com.and.company.db.DBManager;
import com.and.company.entity.Category;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CategoryJDBC implements CategoryDAO {
    private final Logger LOGGER = Logger.getLogger(CategoryJDBC.class);

    private static final String GET_ALL_CATEGORIES_BY_LANG_ID =
        "SELECT category_translation.* from category_translation\n" +
        "INNER JOIN language\n" +
        "ON language.id = category_translation.language_id\n" +
        "WHERE language.name = ?;";

    private static final String GET_COST_BY_KM =
        "SELECT category_translation.cost_by_km from category_translation\n" +
        "INNER JOIN language\n" +
        "ON language.id = category_translation.language_id\n" +
        "WHERE language.id = ? and category_translation.description = ?;";

    private DBManager dbManager = DBManager.getInstance();

    @Override
    public List<Category> getAllCategoriesByLanguage(String languageName) {
        List<Category> categories = new ArrayList<>();
        Mapper<Category> categoryMapper = new CategoryMapper();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(GET_ALL_CATEGORIES_BY_LANG_ID);
            ps.setString(1, languageName);
            rs = ps.executeQuery();
            while (rs.next()) {
                categories.add(categoryMapper.getEntity(rs));
            }
        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }
        return categories;
    }

    @Override
    public BigDecimal getCostPerKm(Long languageId, String description) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        BigDecimal cost = null;
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(GET_COST_BY_KM);
            ps.setLong(1, languageId);
            ps.setString(2, description);
            rs = ps.executeQuery();
            if (rs.next()) {
                cost = rs.getBigDecimal("cost_by_km");
            }
        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }
        return cost;
    }
}
