package com.and.company.dao.implementation;

import com.and.company.bean.ClientOrderBean;
import com.and.company.dao.OrderDAO;
import com.and.company.dao.mapper.OrderTaxiMapper;
import com.and.company.db.DBManager;
import com.and.company.entity.OrderTaxi;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderJDBC implements OrderDAO {
    private final Logger LOGGER = Logger.getLogger(OrderJDBC.class);
    private static final String CREATE_ORDER =
            "INSERT INTO order_taxi (passenger_number, destination_address, pickup_address, cost, account_id, " +
                    "language_id, latitude_destination, longitude_destination, latitude_pickup, longitude_pickup)\n" +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    private static final String GET_ORDERS =
            "SELECT account.name, order_taxi.passenger_number, order_taxi.destination_address,\n" +
            "order_taxi.pickup_address, order_taxi.cost, order_taxi.create_time, language.name AS lang_name\n" +
            "FROM account, order_taxi, language \n" +
            "WHERE account.id = order_taxi.account_id\n" +
            "AND language.id = order_taxi.language_id\n" +
            "LIMIT ?, ?;";

    private static final String GET_ORDERS_SORT_BY_CREATE_TIME_ASC =
            "SELECT account.name, order_taxi.passenger_number, order_taxi.destination_address,\n" +
            "order_taxi.pickup_address, order_taxi.cost, order_taxi.create_time, language.name AS lang_name\n" +
            "FROM account, order_taxi, language \n" +
            "WHERE account.id = order_taxi.account_id\n" +
            "AND language.id = order_taxi.language_id\n" +
            "ORDER BY create_time LIMIT ?,?;";

    private static final String GET_ORDERS_SORT_BY_CREATE_TIME_DESC =
            "SELECT account.name, order_taxi.passenger_number, order_taxi.destination_address,\n" +
            "order_taxi.pickup_address, order_taxi.cost, order_taxi.create_time, language.name AS lang_name\n" +
            "FROM account, order_taxi, language \n" +
            "WHERE account.id = order_taxi.account_id\n" +
            "AND language.id = order_taxi.language_id\n" +
            "ORDER BY create_time DESC LIMIT ?,?;";

    private static final String GET_ORDERS_SORT_BY_COST_ASC =
            "SELECT account.name, order_taxi.passenger_number, order_taxi.destination_address,\n" +
            "order_taxi.pickup_address, order_taxi.cost, order_taxi.create_time, language.name AS lang_name\n" +
            "FROM account, order_taxi, language \n" +
            "WHERE account.id = order_taxi.account_id\n" +
            "AND language.id = order_taxi.language_id\n" +
            "ORDER BY lang_name, cost LIMIT ?,?;";

    private static final String GET_ORDERS_SORT_BY_COST_DESC =
            "SELECT account.name, order_taxi.passenger_number, order_taxi.destination_address,\n" +
            "order_taxi.pickup_address, order_taxi.cost, order_taxi.create_time, language.name AS lang_name\n" +
            "FROM account, order_taxi, language \n" +
            "WHERE account.id = order_taxi.account_id\n" +
            "AND language.id = order_taxi.language_id\n" +
            "ORDER BY lang_name, cost DESC LIMIT ?,?;";

    private static final String GET_ORDERS_FILTER_BETWEEN_DATES =
            "SELECT account.name, order_taxi.passenger_number, order_taxi.destination_address,\n" +
            "order_taxi.pickup_address, order_taxi.cost, order_taxi.create_time, language.name AS lang_name\n" +
            "FROM order_taxi\n" +
            "INNER JOIN account on account.id = order_taxi.account_id\n" +
            "INNER JOIN language on language.id = order_taxi.language_id\n" +
            "WHERE order_taxi.create_time BETWEEN ? AND ?\n" +
            "ORDER BY create_time LIMIT ?,?;";

    private static final String GET_ORDERS_FILTER_START_FROM_DATE =
            "SELECT account.name, order_taxi.passenger_number, order_taxi.destination_address,\n" +
            "order_taxi.pickup_address, order_taxi.cost, order_taxi.create_time, language.name AS lang_name\n" +
            "FROM order_taxi\n" +
            "INNER JOIN account on account.id = order_taxi.account_id\n" +
            "INNER JOIN language on language.id = order_taxi.language_id\n" +
            "WHERE order_taxi.create_time >= ?\n" +
            "ORDER BY create_time LIMIT ?,?;";

    private static final String GET_ORDERS_FILTER_CLIENT_NAME =
            "SELECT account.name, order_taxi.passenger_number, order_taxi.destination_address,\n" +
            "order_taxi.pickup_address, order_taxi.cost, order_taxi.create_time, language.name AS lang_name\n" +
            "FROM order_taxi\n" +
            "INNER JOIN account on account.id = order_taxi.account_id\n" +
            "INNER JOIN language on language.id = order_taxi.language_id\n" +
            "WHERE account.name LIKE ? ESCAPE '!'\n" +
            "ORDER BY account.name LIMIT ?,?;";

    private static final String GET_USER_ORDERS =
            "SELECT * FROM order_taxi WHERE account_id = ?";

    private DBManager dbManager = DBManager.getInstance();

    public boolean createOrder(OrderTaxi orderTaxi) {
        boolean result = false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(CREATE_ORDER, Statement.RETURN_GENERATED_KEYS);
            int k = 1;
            ps.setInt(k++, orderTaxi.getPassengerNumber());
            ps.setString(k++, orderTaxi.getDestinationAddress());
            ps.setString(k++, orderTaxi.getPickupAddress());
            ps.setBigDecimal(k++, orderTaxi.getCost());
            ps.setLong(k++, orderTaxi.getAccountId());
            ps.setLong(k++, orderTaxi.getLanguageId());
            ps.setBigDecimal(k++, orderTaxi.getLatitudeDestination());
            ps.setBigDecimal(k++, orderTaxi.getLongitudeDestionation());
            ps.setBigDecimal(k++, orderTaxi.getLatitudePickup());
            ps.setBigDecimal(k, orderTaxi.getLongitudePickup());

            if (ps.executeUpdate() > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    orderTaxi.setId(rs.getLong(1));
                    result = true;
                }
            }
        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }
        return result;
    }

    public List<ClientOrderBean> getOrders(int from, int to) {
        List<ClientOrderBean> orders = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(GET_ORDERS);
            int k = 1;
            ps.setInt(k++, from);
            ps.setInt(k++, to);
            rs = ps.executeQuery();
            while (rs.next()) {
                orders.add(createBean(rs));
            }

        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }

        return orders;
    }

    public List<ClientOrderBean> getOrdersSortedByCreateTime(int from, int to, boolean isDesc) {
        List<ClientOrderBean> orders = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = dbManager.getConnection();
            if (isDesc) {
                ps = con.prepareStatement(GET_ORDERS_SORT_BY_CREATE_TIME_DESC);
            } else {
                ps = con.prepareStatement(GET_ORDERS_SORT_BY_CREATE_TIME_ASC);
            }

            int k = 1;
            ps.setInt(k++, from);
            ps.setInt(k++, to);
            rs = ps.executeQuery();
            while (rs.next()) {
                orders.add(createBean(rs));
            }

        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }

        return orders;
    }

    public List<ClientOrderBean> getOrdersSortedByCost(int from, int to, boolean isDesc) {
        List<ClientOrderBean> orders = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = dbManager.getConnection();
            if (isDesc) {
                ps = con.prepareStatement(GET_ORDERS_SORT_BY_COST_DESC);
            } else {
                ps = con.prepareStatement(GET_ORDERS_SORT_BY_COST_ASC);
            }

            int k = 1;
            ps.setInt(k++, from);
            ps.setInt(k++, to);
            rs = ps.executeQuery();
            while (rs.next()) {
                orders.add(createBean(rs));
            }

        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }

        return orders;
    }

    public List<ClientOrderBean> getOrdersFilteredBetweenDates(Date fromDate, Date toDate,
                                                         int from, int to) {
        List<ClientOrderBean> orders = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(GET_ORDERS_FILTER_BETWEEN_DATES);

            int k = 1;
            ps.setDate(k++, fromDate);
            ps.setDate(k++, toDate);
            ps.setInt(k++, from);
            ps.setInt(k++, to);
            rs = ps.executeQuery();
            while (rs.next()) {
                orders.add(createBean(rs));
            }

        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }

        return orders;
    }

    public List<ClientOrderBean> getOrdersFilterStartFromDate(Date fromDate,
                                                        int from,
                                                        int to) {
        List<ClientOrderBean> orders = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(GET_ORDERS_FILTER_START_FROM_DATE);

            int k = 1;
            ps.setDate(k++, fromDate);
            ps.setInt(k++, from);
            ps.setInt(k++, to);
            rs = ps.executeQuery();
            while (rs.next()) {
                orders.add(createBean(rs));
            }

        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }

        return orders;
    }

    public List<ClientOrderBean> getOrdersFilterClientName(String name, int from, int to) {
        List<ClientOrderBean> orders = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(GET_ORDERS_FILTER_CLIENT_NAME);

            int k = 1;
            ps.setString(k++, escape(name) + "%");
            ps.setInt(k++, from);
            ps.setInt(k++, to);
            rs = ps.executeQuery();
            while (rs.next()) {
                orders.add(createBean(rs));
            }

        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }

        return orders;
    }

    @Override
    public List<OrderTaxi> getUserOrders(long id) {
        List<OrderTaxi> orders = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        OrderTaxiMapper mapper = new OrderTaxiMapper();
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(GET_USER_ORDERS);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                orders.add(mapper.getEntity(rs));
            }

        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }

        return orders;
    }

    private ClientOrderBean createBean(ResultSet rs) throws SQLException {
        return new ClientOrderBean(
                rs.getString("name"),
                rs.getInt("passenger_number"),
                rs.getString("destination_address"),
                rs.getString("pickup_address"),
                rs.getBigDecimal("cost"),
                rs.getTimestamp("create_time"),
                rs.getString("lang_name")
        );
    }

    static String escape(String param) {
        return param.replace("!", "!!").replace("%", "!%").replace("_", "!_").replace("[", "![");
    }
}