package com.and.company.dao.implementation;

import com.and.company.dao.AccountDAO;
import com.and.company.db.DBManager;
import com.and.company.entity.Account;
import org.apache.log4j.Logger;

import java.sql.*;

public class AccountJDBC implements AccountDAO {
    private final Logger LOGGER = Logger.getLogger(AccountJDBC.class);

    private static final String CREATE_ACCOUNT =
            "INSERT INTO account (email, password, name) VALUES (?, ?, ?);";

    private static final String GET_ACCOUNT =
            "SELECT account.*, role.name AS role_name FROM account\n" +
                    "INNER JOIN role\n" +
                    "ON account.role_id = role.id\n" +
                    "WHERE account.email = ?;";

    private DBManager dbManager = DBManager.getInstance();

    public boolean createClient(Account account) {
        boolean result = false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(CREATE_ACCOUNT, Statement.RETURN_GENERATED_KEYS);
            int k = 1;
            ps.setString(k++, account.getEmail());
            ps.setString(k++, account.getPassword());
            ps.setString(k++, account.getName());

            if (ps.executeUpdate() > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    account.setId(rs.getLong(1));
                    result = true;
                }
            }
        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }

        return result;
    }

    public Account getAccount(String email) {
        Account account = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            ps = con.prepareStatement(GET_ACCOUNT, Statement.RETURN_GENERATED_KEYS);
            int k = 1;
            ps.setString(k++, email);
            rs = ps.executeQuery();
            if (rs.next()) {
                account = Account.create(
                        rs.getLong("id"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getString("name"),
                        rs.getTimestamp("create_time"),
                        rs.getLong("role_id"),
                        rs.getLong("loyalty_id")
                );
            }
        } catch (SQLException ex) {
            LOGGER.error(ex);
        } finally {
            dbManager.close(rs, ps, con);
        }

        return account;
    }
}
