package com.and.company.dao;

import com.and.company.bean.ClientOrderBean;
import com.and.company.entity.OrderTaxi;

import java.sql.Date;
import java.util.List;

public interface OrderDAO {
    boolean createOrder(OrderTaxi orderTaxi);
    List<ClientOrderBean> getOrders(int from, int to);
    List<ClientOrderBean> getOrdersSortedByCreateTime(int from, int to, boolean isDesc);
    List<ClientOrderBean> getOrdersSortedByCost(int from, int to, boolean isDesc);
    List<ClientOrderBean> getOrdersFilteredBetweenDates(Date fromDate, Date toDate,
                                                        int from, int to);
    List<ClientOrderBean> getOrdersFilterStartFromDate(Date fromDate,
                                                       int from,
                                                       int to);
    List<ClientOrderBean> getOrdersFilterClientName(String name, int from, int to);
    List<OrderTaxi> getUserOrders(long id);
}
