package com.and.company.dao;

import com.and.company.entity.Car;
import com.and.company.entity.Status;
import java.util.List;

public interface CarDAO {
    Car getAvailableCarByCapacityAndStatus(Long languageId,
                                           Integer capacity,
                                           Status status,
                                           String category);
    List<Car> getSeveralCarsByCapacity(Long languageId,
                                       Integer capacity,
                                       Status status,
                                       String category);
    void updateCarStatus(Long carId, Status status);
}
