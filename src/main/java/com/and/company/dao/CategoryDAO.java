package com.and.company.dao;

import com.and.company.entity.Category;

import java.math.BigDecimal;
import java.util.List;

public interface CategoryDAO {
    List<Category> getAllCategoriesByLanguage(String languageName);
    BigDecimal getCostPerKm(Long languageId, String description);
}
