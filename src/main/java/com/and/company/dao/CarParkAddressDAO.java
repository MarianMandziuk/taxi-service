package com.and.company.dao;

import com.and.company.entity.CarParkAddress;

public interface CarParkAddressDAO {
    void updateAddress(CarParkAddress address);
    CarParkAddress getCarParkAddress();
}
