package com.and.company.dao.mapper;

import com.and.company.entity.OrderTaxi;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderTaxiMapper implements Mapper<OrderTaxi> {

    @Override
    public OrderTaxi getEntity(ResultSet resultSet) throws SQLException {
        OrderTaxi orderTaxi = new OrderTaxi();
        orderTaxi.setId(resultSet.getLong("id"));
        orderTaxi.setPassengerNumber(resultSet.getInt("passenger_number"));
        orderTaxi.setDestinationAddress(resultSet.getString("destination_address"));
        orderTaxi.setPickupAddress(resultSet.getString("pickup_address"));
        orderTaxi.setCost(resultSet.getBigDecimal("cost"));
        orderTaxi.setCreateTime(resultSet.getTimestamp("create_time"));
        orderTaxi.setAccountId(resultSet.getLong("account_id"));
        orderTaxi.setLanguageId(resultSet.getLong("language_id"));
        orderTaxi.setLatitudeDestination(resultSet.getBigDecimal("latitude_destination"));
        orderTaxi.setLongitudeDestionation(resultSet.getBigDecimal("longitude_destination"));
        orderTaxi.setLatitudePickup(resultSet.getBigDecimal("latitude_pickup"));
        orderTaxi.setLongitudePickup(resultSet.getBigDecimal("longitude_pickup"));
        return orderTaxi;
    }
}
