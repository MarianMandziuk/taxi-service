package com.and.company.dao.mapper;

import com.and.company.entity.Category;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryMapper implements Mapper<Category> {
    @Override
    public Category getEntity(ResultSet resultSet) throws SQLException {
        Category category = new Category();
        category.setId(resultSet.getLong("id"));
        category.setDescription(resultSet.getString("description"));
        category.setCostPerKM(resultSet.getBigDecimal("cost_by_km"));
        category.setLanguageId(resultSet.getInt("language_id"));
        return category;
    }
}
