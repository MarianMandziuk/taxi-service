package com.and.company.dao.mapper;

import com.and.company.entity.Loyalty;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LoyaltyMapper implements Mapper<Loyalty> {

    @Override
    public Loyalty getEntity(ResultSet resultSet) throws SQLException {
        Loyalty loyalty = new Loyalty();
        loyalty.setId(resultSet.getLong("id"));
        loyalty.setDiscount(resultSet.getInt("discount"));
        loyalty.setPrice(resultSet.getBigDecimal("price"));
        loyalty.setLanguageId(resultSet.getLong("language_id"));
        return loyalty;
    }
}
