package com.and.company.dao;

import com.and.company.entity.Loyalty;

public interface LoyaltyDAO {
    Loyalty getLoyalty(Long id);
}
