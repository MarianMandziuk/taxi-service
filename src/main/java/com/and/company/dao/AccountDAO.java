package com.and.company.dao;

import com.and.company.entity.Account;

public interface AccountDAO {
    boolean createClient(Account account);
    Account getAccount(String email);
}
