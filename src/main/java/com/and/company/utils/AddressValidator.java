package com.and.company.utils;

public class AddressValidator {
    private static final double LATITUDE_LOWER_BOUND = -90;
    private static final double LATITUDE_UPPER_BOUND = 90;
    private static final double LONGITUDE_LOWER_BOUND = -180;
    private static final double LONGITUDE_UPPER_BOUND = 180;

    private double lat1;
    private double lon1;
    private double lat2;
    private double lon2;

    public AddressValidator(double lat1, double lon1, double lat2, double lon2) {
        this.lat1 = lat1;
        this.lon1 = lon1;
        this.lat2 = lat2;
        this.lon2 = lon2;
    }

    public boolean validateRange() {
        return  ((lat1 >= LATITUDE_LOWER_BOUND && lat1 <= LATITUDE_UPPER_BOUND)
                    && (lat2 >= LATITUDE_LOWER_BOUND && lat2 <= LATITUDE_UPPER_BOUND)
                    && (lon1 >= LONGITUDE_LOWER_BOUND && lon1 <= LONGITUDE_UPPER_BOUND)
                    && (lon2 >= LONGITUDE_LOWER_BOUND && lon2 <= LONGITUDE_UPPER_BOUND));
    }

    public boolean validateIsSame() {
        return lat1 == lat2 && lon1 == lon2;
    }
}
