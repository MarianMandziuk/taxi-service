package com.and.company.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

public class CurrencyLang {
    public static final String UKRAINE = "ua";
    public static final String USA = "en";
    private static final Locale ukraine = new Locale("uk", "UA");
    private static final Locale usa = new Locale("en", "US");

    private static Locale getLocale(String lang) {
        if (USA.equals(lang)) {
            return usa;
        }
        return ukraine;
    }

    public static String formatCurrency(String lang, BigDecimal moneyValue) {
        Locale locale = getLocale(lang);
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
        return numberFormat.format(moneyValue);
    }
}
