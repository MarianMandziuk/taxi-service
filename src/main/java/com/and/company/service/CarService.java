package com.and.company.service;

import com.and.company.dao.CarDAO;
import com.and.company.dao.FactoryDAO;
import com.and.company.entity.Car;
import com.and.company.entity.Status;

import java.util.List;

public class CarService {
    private FactoryDAO factoryDAO = FactoryDAO.getInstance();
    private CarDAO carDAO = factoryDAO.createCarDAO();

    public Car getAvailableCarByCapacityAndStatus(Long languageId,
                                                  Integer capacity, Status status, String category) {
        return carDAO.getAvailableCarByCapacityAndStatus(languageId, capacity, status, category);
    }

    public List<Car> getSeveralCarsByCapacity(Long languageId,
                                              Integer capacity,
                                              Status status,
                                              String category) {
        return carDAO.getSeveralCarsByCapacity(languageId, capacity, status, category);
    }

    public void updateCarStatus(Long carId, Status carStatus) {
        carDAO.updateCarStatus(carId, carStatus);
    }
}
