package com.and.company.service;

import com.and.company.dao.CarParkAddressDAO;
import com.and.company.dao.FactoryDAO;
import com.and.company.entity.CarParkAddress;

public class ParkAddressService {
    private FactoryDAO factoryDAO = FactoryDAO.getInstance();
    private CarParkAddressDAO carParkAddressDAO = factoryDAO.createParkAddressDAO();

    public void updateAddress(CarParkAddress address) {
        carParkAddressDAO.updateAddress(address);
    }

    public CarParkAddress getCarParkAddress() {
        return carParkAddressDAO.getCarParkAddress();
    }
}
