package com.and.company.service;

import com.and.company.bean.ClientOrderBean;
import com.and.company.dao.FactoryDAO;
import com.and.company.dao.OrderDAO;
import com.and.company.entity.OrderTaxi;

import java.sql.Date;
import java.util.List;

public class OrderService {
    private FactoryDAO factoryDAO = FactoryDAO.getInstance();
    private OrderDAO orderDAO = factoryDAO.createOrderDAO();

    public boolean createOrder(OrderTaxi order) {
        return orderDAO.createOrder(order);
    }

    public List<ClientOrderBean> getOrders(int from, int to) {
        return orderDAO.getOrders(from, to);
    }

    public List<ClientOrderBean> getOrdersByTime(int from, int number, boolean isDesc) {
        return orderDAO.getOrdersSortedByCreateTime(from, number, isDesc);
    }

    public List<ClientOrderBean> getOrdersByCost(int from, int number, boolean isDesc) {
        return orderDAO.getOrdersSortedByCost(from, number, isDesc);
    }

    public List<ClientOrderBean> getOrdersFilteredBetweenDates(Date fromDate, Date toDate,
                                                               int from, int number) {
        return orderDAO.getOrdersFilteredBetweenDates(fromDate, toDate, from, number);
    }

    public List<ClientOrderBean> getOrdersFilterClientName(String name, int from, int number) {
        return orderDAO.getOrdersFilterClientName(name, from, number);
    }

    public List<OrderTaxi> getUserOrders(long userId){
        return orderDAO.getUserOrders(userId);
    }
}
