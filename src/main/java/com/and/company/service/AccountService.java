package com.and.company.service;

import com.and.company.dao.AccountDAO;
import com.and.company.dao.FactoryDAO;
import com.and.company.dao.LanguageDAO;
import com.and.company.dao.LoyaltyDAO;
import com.and.company.entity.Account;
import com.and.company.entity.Language;
import com.and.company.entity.Loyalty;

public class AccountService {
    private FactoryDAO factoryDAO = FactoryDAO.getInstance();
    private AccountDAO accountDAO = factoryDAO.createAccountDAO();
    private LoyaltyDAO loyaltyDAO = factoryDAO.createLoyaltyDAO();
    private LanguageDAO languageDAO = factoryDAO.createLanguageDAO();

    public boolean createClient(Account account) {
        return accountDAO.createClient(account);
    }

    public Account getAccount(String email) {
        return accountDAO.getAccount(email);
    }

    public Loyalty getAccountLoyalty(Long id) {
        return loyaltyDAO.getLoyalty(id);
    }

    public Language getLanguageByName(String langName) {
        return languageDAO.getLanguage(langName);
    }
}
