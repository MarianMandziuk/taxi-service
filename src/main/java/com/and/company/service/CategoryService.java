package com.and.company.service;

import com.and.company.dao.CategoryDAO;
import com.and.company.dao.FactoryDAO;
import com.and.company.entity.Category;

import java.math.BigDecimal;
import java.util.List;

public class CategoryService {
    private FactoryDAO factoryDAO = FactoryDAO.getInstance();
    private CategoryDAO categoryDAO = factoryDAO.createCategoryDAO();

    public List<Category> getAllCategoriesByLanguage(String languageName) {
        return categoryDAO.getAllCategoriesByLanguage(languageName);
    }

    public BigDecimal getCostByKm(Long languageId, String categoryDescription) {
        return categoryDAO.getCostPerKm(languageId, categoryDescription);
    }
}
