package com.and.company.bean;

import com.and.company.entity.Entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class ClientOrderBean extends Entity {
    private String userName;
    private Integer passengerNumber;
    private String destinationAddress;
    private String pickupAddress;
    private BigDecimal cost;
    private Timestamp createTime;
    private String language;

    public ClientOrderBean(String userName, Integer passengerNumber,
                           String destinationAddress, String pickupAddress,
                           BigDecimal cost, Timestamp createTime,
                           String language) {
        this.userName = userName;
        this.passengerNumber = passengerNumber;
        this.destinationAddress = destinationAddress;
        this.pickupAddress = pickupAddress;
        this.cost = cost;
        this.createTime = createTime;
        this.language = language;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getPassengerNumber() {
        return passengerNumber;
    }

    public void setPassengerNumber(Integer passengerNumber) {
        this.passengerNumber = passengerNumber;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(String pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return "UserOrderBean{" +
                "userName='" + userName + '\'' +
                ", passengerNumber=" + passengerNumber +
                ", destinationAddress='" + destinationAddress + '\'' +
                ", pickupAddress='" + pickupAddress + '\'' +
                ", cost=" + cost +
                ", createTime=" + createTime +
                ", language=" + language +
                '}';
    }
}
