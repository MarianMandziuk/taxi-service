package com.and.company.entity;

import java.math.BigDecimal;

public class Category extends Entity {
    private String description;
    private BigDecimal costPerKM;
    private Integer languageId;

    public static Category create(Long id,
                                  String description,
                                  BigDecimal costPerKM,
                                  Integer languageId) {
        Category category = new Category();
        category.setId(id);
        category.setDescription(description);
        category.setCostPerKM(costPerKM);
        category.setLanguageId(languageId);
        return category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getCostPerKM() {
        return costPerKM;
    }

    public void setCostPerKM(BigDecimal costPerKM) {
        this.costPerKM = costPerKM;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id='" + getId() + '\'' +
                ", description='" + description + '\'' +
                ", costPerMile=" + costPerKM +
                ", languageId='" + languageId + '\'' +
                '}';
    }
}
