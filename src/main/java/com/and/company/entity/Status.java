package com.and.company.entity;

public enum Status {
    AVAILABLE, INACTIVE, BUSY;

    public static Status getStatus(String statusName) {
        return Status.valueOf(statusName.toUpperCase());
    }
}
