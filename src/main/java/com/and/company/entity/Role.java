package com.and.company.entity;

public enum Role {
    ADMIN(1), CUSTOMER(2);

    public static Role getRole(String roleName) {
        return Role.valueOf(roleName.toUpperCase());
    }

    int roleId;

    Role(int roleId) {
        this.roleId = roleId;
    }

    public int getRoleId() {
        return roleId;
    }
}
