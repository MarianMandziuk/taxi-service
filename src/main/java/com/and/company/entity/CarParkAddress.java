package com.and.company.entity;

import java.math.BigDecimal;

public class CarParkAddress extends Entity {
    private String address;
    private BigDecimal longitude;
    private BigDecimal latitude;

    public CarParkAddress() {
    }

    public CarParkAddress(String address, BigDecimal longitude, BigDecimal latitude) {
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "CarParkAddress{" +
                "id='" + getId() + '\'' +
                ", address='" + address + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }
}
