package com.and.company.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class OrderTaxi extends Entity {
    private Integer passengerNumber;
    private String destinationAddress;
    private String pickupAddress;
    private BigDecimal cost;
    private Timestamp createTime;
    private Long accountId;
    private Long languageId;
    private BigDecimal latitudeDestination;
    private BigDecimal longitudeDestionation;
    private BigDecimal latitudePickup;
    private BigDecimal longitudePickup;

    public static OrderTaxi create(Long id,
                                   Integer passengerNumber,
                                   String destinationAddress,
                                   String pickupAddress,
                                   BigDecimal cost,
                                   Timestamp createTime,
                                   Long accountId,
                                   Long languageId,
                                   BigDecimal latitudeDestination,
                                   BigDecimal longitudeDestionation,
                                   BigDecimal latitudePickup,
                                   BigDecimal longitudePickup) {
        OrderTaxi orderTaxi = new OrderTaxi();
        orderTaxi.setId(id);
        orderTaxi.setPassengerNumber(passengerNumber);
        orderTaxi.setDestinationAddress(destinationAddress);
        orderTaxi.setPickupAddress(pickupAddress);
        orderTaxi.setCreateTime(createTime);
        orderTaxi.setAccountId(accountId);
        orderTaxi.setLanguageId(languageId);
        orderTaxi.setCost(cost);
        orderTaxi.setLatitudeDestination(latitudeDestination);
        orderTaxi.setLongitudeDestionation(longitudeDestionation);
        orderTaxi.setLatitudePickup(latitudePickup);
        orderTaxi.setLongitudePickup(longitudePickup);
        return orderTaxi;
    }

    public Integer getPassengerNumber() {
        return passengerNumber;
    }

    public void setPassengerNumber(Integer passengerNumber) {
        this.passengerNumber = passengerNumber;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(String pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public BigDecimal getLatitudeDestination() {
        return latitudeDestination;
    }

    public void setLatitudeDestination(BigDecimal latitudeDestination) {
        this.latitudeDestination = latitudeDestination;
    }

    public BigDecimal getLongitudeDestionation() {
        return longitudeDestionation;
    }

    public void setLongitudeDestionation(BigDecimal longitudeDestionation) {
        this.longitudeDestionation = longitudeDestionation;
    }

    public BigDecimal getLatitudePickup() {
        return latitudePickup;
    }

    public void setLatitudePickup(BigDecimal latitudePickup) {
        this.latitudePickup = latitudePickup;
    }

    public BigDecimal getLongitudePickup() {
        return longitudePickup;
    }

    public void setLongitudePickup(BigDecimal longitudePickup) {
        this.longitudePickup = longitudePickup;
    }

    @Override
    public String toString() {
        return "OrderTaxi{" +
                "id='" + getId() + '\'' +
                ", passengerNumber=" + passengerNumber +
                ", destinationAddress='" + destinationAddress + '\'' +
                ", pickupAddress='" + pickupAddress + '\'' +
                ", cost=" + cost +
                ", createTime=" + createTime +
                ", accountId=" + accountId +
                ", languageId=" + languageId +
                ", latitudeDestination=" + latitudeDestination +
                ", longitudeDestionation=" + longitudeDestionation +
                ", latitudePickup=" + latitudePickup +
                ", longitudePickup=" + longitudePickup +
                '}';
    }
}
