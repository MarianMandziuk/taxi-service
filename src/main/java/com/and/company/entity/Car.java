package com.and.company.entity;

public class Car extends Entity {
    private String modelName;
    private Integer capacity;
    private Long statusId;

    public static Car create(Long id,
                             String modelName,
                             Integer capacity,
                             Long statusId) {
        Car car = new Car();
        car.setId(id);
        car.setModelName(modelName);
        car.setCapacity(capacity);
        car.setStatusId(statusId);
        return car;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id='" + getId() + '\'' +
                ", modelName='" + modelName + '\'' +
                ", capacity=" + capacity +
                ", statusId=" + statusId +
                '}';
    }
}
