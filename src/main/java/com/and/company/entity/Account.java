package com.and.company.entity;

import java.sql.Timestamp;

public class Account extends Entity {
    private String email;
    private String password;
    private String name;
    private Timestamp createTime;
    private Long roleId;
    private Long loyaltyId;

    public static Account create(Long id, String email, String password,
                                 String name, Timestamp createTime, Long roleId,
                                 Long loyaltyId) {
        Account account = new Account();
        account.setId(id);
        account.setEmail(email);
        account.setPassword(password);
        account.setName(name);
        account.setCreateTime(createTime);
        account.setRoleId(roleId);
        account.setLoyaltyId(loyaltyId);
        return account;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getLoyaltyId() {
        return loyaltyId;
    }

    public void setLoyaltyId(Long loyaltyId) {
        this.loyaltyId = loyaltyId;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id='" + getId() + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", createTime=" + createTime +
                ", roleId=" + roleId +
                ", loyaltyId=" + loyaltyId +
                '}';
    }
}
