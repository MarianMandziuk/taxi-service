package com.and.company.entity;

import java.math.BigDecimal;

public class Loyalty extends Entity {
    private BigDecimal price;
    private Integer discount;
    private Long languageId;

    public Loyalty() {
    }

    public Loyalty(Long id, BigDecimal price,
                   Integer discount, Long languageId) {
        super(id);
        this.price = price;
        this.discount = discount;
        this.languageId = languageId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    @Override
    public String toString() {
        return "Loyalty{" +
                "id='" + getId() + '\'' +
                ", price=" + price +
                ", discount=" + discount +
                ", languageId=" + languageId +
                '}';
    }
}
