package com.and.company.controller;

import com.and.company.entity.Account;
import com.and.company.entity.Role;
import com.and.company.service.AccountService;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import org.apache.log4j.Logger;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private final Logger LOGGER = Logger.getLogger(LoginServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        LOGGER.info("Login get method call.");

        String command = "views/login.jsp";
        if (req.getSession() != null && req.getSession().getAttribute("account") != null){
            Account account = (Account) req.getSession().getAttribute("account");
            long roleId = account.getRoleId();
            if (roleId == Role.CUSTOMER.getRoleId()) {
                command = "/create_order";
            } else if (roleId == Role.ADMIN.getRoleId()) {
                command = "/info_page";
            }
            LOGGER.info("Redirect to " + command + " page");
            resp.sendRedirect(command);
        } else {
            LOGGER.info("Forward to " + command + " page");
            req.getRequestDispatcher(command).forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        LOGGER.info("doPost called");
        req.getSession().setAttribute("errorMessagePassword", null);
        req.getSession().setAttribute("errorMessageEmail", null);
        String email = req.getParameter("email");
        String passwordToHash = req.getParameter("password");

        AccountService accountService = new AccountService();
        Account account = accountService.getAccount(email);
        String command = "/login";
        if (account != null) {
            if (account.getPassword().equals(passwordToHash)) {
                HttpSession session = req.getSession();
                session.setAttribute("account", account);

                long roleId = account.getRoleId();
                if (roleId == Role.ADMIN.getRoleId()) {
                    command = "/orders";
                } else {
                    command = "/create_order";
                }
                LOGGER.info("successful login");
            } else {
                req.getSession().setAttribute("errorMessagePassword", "Invalid password");
            }
        } else {
            req.getSession().setAttribute("errorMessageEmail", "Invalid email");
        }
        LOGGER.info("Account " + account + " redirected: " + command);
        resp.sendRedirect(command);
    }
}
