package com.and.company.controller;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/order_status")
public class OrderStatusServlet extends HttpServlet {
    private final Logger LOGGER = Logger.getLogger(OrderStatusServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("Order status called.");
        String orderCompleteMessage = (String) req.getSession().getAttribute("orderCompleted");
        String orderFailedMessage = (String) req.getSession().getAttribute("orderFailed");
        if (orderCompleteMessage == null && orderFailedMessage == null) {
            req.getRequestDispatcher("/create_order").forward(req, resp);
        } else {
            req.getRequestDispatcher("views/order_status.jsp").forward(req, resp);
        }
        req.getSession().setAttribute("orderCompleted", null);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }
}
