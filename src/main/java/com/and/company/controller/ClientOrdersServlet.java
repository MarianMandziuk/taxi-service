package com.and.company.controller;

import com.and.company.entity.Account;
import com.and.company.entity.OrderTaxi;
import com.and.company.service.OrderService;
import java.util.List;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/client_orders")
public class ClientOrdersServlet extends HttpServlet {
    private final Logger LOGGER =  Logger.getLogger(ClientOrdersServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("Client orders method get has been called.");

        Account account = (Account) req.getSession().getAttribute("account");
        long accountId = account.getId();
        OrderService orderService = new OrderService();
        List<OrderTaxi> orders = orderService.getUserOrders(accountId);
        req.setAttribute("orders", orders);
        req.getRequestDispatcher("views/client_orders.jsp").forward(req, resp);
    }
}
