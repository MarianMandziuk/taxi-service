package com.and.company.controller;

import com.and.company.entity.Account;
import com.and.company.service.AccountService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

@WebServlet("/signup")
public class SignupServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("views/signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String password = request.getParameter("password");
        String repeatPassword = request.getParameter("password_repeat");

        request.getSession().setAttribute("errorPasswordRepeat", null);
        request.getSession().setAttribute("errorMessageSignup", null);
        request.getSession().setAttribute("newUserName", null);

        if (password == null || !password.equals(repeatPassword)) {
            request.getSession().setAttribute("errorPasswordRepeat", "Invalid password.");
            response.sendRedirect("/signup");
            return;
        }

        AccountService userService = new AccountService();
        Account user = new Account();
        user.setEmail(request.getParameter("email"));
        user.setPassword(repeatPassword);
        user.setName(request.getParameter("name"));
        if (!userService.createClient(user)) {
            request.getSession().setAttribute("errorMessageSignup", "Invalid data or user with this email available.");
            response.sendRedirect("/signup");
        } else {
            request.getSession().setAttribute("newUserName",  user.getName());
            response.sendRedirect("/signup");
        }
    }
}
