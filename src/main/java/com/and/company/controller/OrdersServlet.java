package com.and.company.controller;

import com.and.company.bean.ClientOrderBean;
import com.and.company.service.OrderService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

@WebServlet("/orders")
public class OrdersServlet extends HttpServlet {
    private final Logger LOGGER = Logger.getLogger(OrdersServlet.class);
    private static final int ITEMS_ON_PAGE = 10;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("Orders' get method work");
        OrderService orderService = new OrderService();
        int from;
        int number = ITEMS_ON_PAGE;
        if (req.getParameter("from") == null) {
            from = 1;
        } else {
            from = Integer.parseInt(req.getParameter("from"));
        }
        List<ClientOrderBean> orders;

        if (req.getParameter("create_time") != null && req.getParameter("isDesc") != null) {
            boolean isDesc = req.getParameter("isDesc").equals("true");
            orders = orderService.getOrdersByTime(from, number, isDesc);
            req.setAttribute("isDesc", isDesc);
            req.setAttribute("create_time", true);
        } else if (req.getParameter("cost") != null && req.getParameter("isDesc") != null) {
            boolean isDesc = req.getParameter("isDesc").equals("true");
            orders = orderService.getOrdersByCost(from, number, isDesc);
            req.setAttribute("isDesc", isDesc);
            req.setAttribute("cost", true);
        } else if (req.getParameter("filter_date") != null
                && req.getParameter("from_date") != null
                && req.getParameter("to_date") != null) {
            Date fromDate = Date.valueOf(req.getParameter("from_date"));
            Date toDate = Date.valueOf(req.getParameter("to_date"));
            orders = orderService.getOrdersFilteredBetweenDates(fromDate, toDate, from, number);
            req.setAttribute("from_date", fromDate);
            req.setAttribute("to_date", toDate);
            req.setAttribute("filter_date", true);
        } else if (req.getParameter("clientname") != null) {
            String clientname =req.getParameter("clientname");
            orders = orderService.getOrdersFilterClientName(clientname, from, number);
            req.setAttribute("clientname", clientname);
        } else {
            orders = orderService.getOrders(from, number);
        }

        req.setAttribute("orders", orders);
        req.setAttribute("from", from);
        req.setAttribute("number", number);
        req.getRequestDispatcher("views/orders.jsp").forward(req, resp);
    }
}
