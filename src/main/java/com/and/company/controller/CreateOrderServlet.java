package com.and.company.controller;

import com.and.company.entity.*;
import com.and.company.service.*;
import com.and.company.utils.AddressValidator;
import com.and.company.utils.CurrencyLang;
import com.and.company.utils.Distance;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static com.and.company.utils.Distance.minutesSpend;

@WebServlet("/create_order")
public class CreateOrderServlet extends HttpServlet {
    private final Logger LOGGER =  Logger.getLogger(CreateOrderServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("Create order method get has been called.");

        Account account = (Account) req.getSession().getAttribute("account");
        if (account == null) {
            LOGGER.info("Without account forward to login");
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            long roleId = account.getRoleId();
            if (roleId == Role.ADMIN.getRoleId()) {
                LOGGER.info("Forward to info page");
                req.getRequestDispatcher("views/info_page").forward(req, resp);
            } else {
                LOGGER.info("Forward to create order page");
                CategoryService categoryService = new CategoryService();
                String lang = (String) req.getSession().getAttribute("lang");
                AccountService accountService = new AccountService();
                Language language = accountService.getLanguageByName(lang);
                List<Category> categories = categoryService.getAllCategoriesByLanguage(language.getName());
                req.setAttribute("categories", categories);
                req.getRequestDispatcher("views/create_order.jsp").forward(req, resp);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("Create order method post");

        clearOrderSessionData(req);
        if (req.getParameter("orderSeveralCar") != null) {
            orderSeveralCars(req, resp);
            return;
        }
        orderCar(req, resp);

    }

    private void orderCar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("Ordering one car.");

        OrderService orderService = new OrderService();
        CarService carService = new CarService();
        AccountService accountService = new AccountService();

        String lang = (String) req.getSession().getAttribute("lang");
        Language language = accountService.getLanguageByName(lang);
        String capacityStr = req.getParameter("capacity");
        int capacity = 0;
        if (capacityStr != null) {
            try {
                capacity = Integer.parseInt(capacityStr);
            } catch (NumberFormatException e) {
                LOGGER.info("Incorrect capacity, ", e);
            }
        }
        String category = req.getParameter("category");
        Car car = carService.getAvailableCarByCapacityAndStatus(language.getId(), capacity, Status.AVAILABLE, category);

        if (car != null) {
            LOGGER.info("Validating coordinates.");
            BigDecimal lonD;
            BigDecimal latD;
            BigDecimal lonP;
            BigDecimal latP ;
            try {
                lonD = new BigDecimal(req.getParameter("longitude_destination"));
                latD = new BigDecimal(req.getParameter("latitude_destination"));
                lonP = new BigDecimal(req.getParameter("longitude_pickup"));
                latP = new BigDecimal(req.getParameter("latitude_pickup"));
            } catch (NumberFormatException e) {
                LOGGER.info("Incorrect coordinates format.");
                addSessionData(req, capacity);
                req.getSession().setAttribute("incorrectData", "Incorrect input data");
                resp.sendRedirect("/create_order");
                return;
            }
            AddressValidator validator = new AddressValidator(latD.doubleValue(),
                    lonD.doubleValue(),
                    latP.doubleValue(),
                    lonP.doubleValue());
            if (!validator.validateRange()) {
                LOGGER.info("Invalid range coordinates.");
                addSessionData(req, capacity);
                req.getSession().setAttribute("coordinatesOutOfRange", "Incorrect coordinates");
                resp.sendRedirect("/create_order");
                return;
            }
            if (validator.validateIsSame()) {
                LOGGER.info("Invalid. Coordinates are same.");
                addSessionData(req, capacity);
                req.getSession().setAttribute("sameCoordinates", "Can't be same address coordinates");
                resp.sendRedirect("/create_order");
                return;
            }

            CategoryService categoryService = new CategoryService();

            OrderTaxi orderTaxi = new OrderTaxi();
            Account account = (Account) req.getSession().getAttribute("account");
            orderTaxi.setAccountId(account.getId());
            orderTaxi.setPassengerNumber(capacity);
            orderTaxi.setDestinationAddress(req.getParameter("destinationAddress"));
            orderTaxi.setPickupAddress(req.getParameter("pickupAddress"));
            orderTaxi.setLanguageId(language.getId());

            orderTaxi.setLongitudeDestionation(lonD);
            orderTaxi.setLatitudeDestination(latD);
            orderTaxi.setLongitudePickup(lonP);
            orderTaxi.setLatitudePickup(latP);

            BigDecimal costByKm = categoryService.getCostByKm(language.getId(),
                    req.getParameter("category"));
            int km = Distance.getDistanceFromLatLonInKm(latD.doubleValue(), lonD.doubleValue(), latP.doubleValue(), lonD.doubleValue());

            Loyalty loyalty = accountService.getAccountLoyalty(account.getLoyaltyId());
            BigDecimal cost;
            if (loyalty != null && loyalty.getDiscount() > 0) {
                cost = calculateLoyaltyCost(km, loyalty.getDiscount(), costByKm);
            } else {
                cost = calculateCost(km, costByKm);
            }

            orderTaxi.setCost(cost);
            orderService.createOrder(orderTaxi);

            carService.updateCarStatus(car.getId(), Status.BUSY);

            LOGGER.info("Order complete with cost: " + cost);

            String costFormatted = CurrencyLang.formatCurrency(lang, cost);

            ParkAddressService parkAddressService = new ParkAddressService();
            CarParkAddress parkAddress = parkAddressService.getCarParkAddress();
            int waitKm = Distance.getDistanceFromLatLonInKm(latP.doubleValue(),
                    lonD.doubleValue(),
                    parkAddress.getLatitude().doubleValue(),
                    parkAddress.getLongitude().doubleValue());
            int minutes = minutesSpend(waitKm);

            req.getSession().setAttribute("orderCompleted",
                    "Order Completed. You car is " + car.getModelName() + "." +
                            "Wait it for " + minutes + " minutes at " + orderTaxi.getPickupAddress() + ". \n" +
                            "Cost is " + costFormatted);
            LOGGER.info("Complete order. Redirect with to order status.");
            resp.sendRedirect("/order_status");
        } else {
            LOGGER.info("Order for one car has been failed.");
            addSessionData(req, capacity);

            req.getSession().setAttribute("findAnotherOrder",
                    "There is currently no car available according to your settings." +
                            " You can try to find a car of another category," +
                            " or find several cars for this order.");
            LOGGER.info("Redirect to the create order.");
            resp.sendRedirect("/create_order");
        }
    }

    private void orderSeveralCars(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("Ordering several cars.");

        OrderService orderService = new OrderService();
        CarService carService = new CarService();
        AccountService accountService = new AccountService();

        String lang = (String) req.getSession().getAttribute("lang");
        Language language = accountService.getLanguageByName(lang);
        String capacityStr = req.getParameter("capacity");
        int capacity = 0;
        if (capacityStr != null) {
            try {
                capacity = Integer.parseInt(capacityStr);
            } catch (NumberFormatException e) {
                LOGGER.info("Incorrect capacity, ", e);
            }
        }

        String category = req.getParameter("category");

        List<Car> cars = carService.getSeveralCarsByCapacity(language.getId(), capacity, Status.AVAILABLE, category);
        if (cars.size() > 1) {
            LOGGER.info("Validation coorinates.");
            BigDecimal lonD;
            BigDecimal latD;
            BigDecimal lonP;
            BigDecimal latP ;
            try {
                lonD = new BigDecimal(req.getParameter("longitude_destination"));
                latD = new BigDecimal(req.getParameter("latitude_destination"));
                lonP = new BigDecimal(req.getParameter("longitude_pickup"));
                latP = new BigDecimal(req.getParameter("latitude_pickup"));
            } catch (NumberFormatException e) {
                LOGGER.info("Incorrect coordinates format.");
                addSessionData(req, capacity);
                req.getSession().setAttribute("incorrectData", "Incorrect input data");
                resp.sendRedirect("/create_order");
                return;
            }
            AddressValidator validator = new AddressValidator(latD.doubleValue(),
                    lonD.doubleValue(),
                    latP.doubleValue(),
                    lonP.doubleValue());
            if (!validator.validateRange()) {
                addSessionData(req, capacity);
                req.getSession().setAttribute("coordinatesOutOfRange", "Incorrect coordinates");
                resp.sendRedirect("/create_order");
                return;
            }
            if (validator.validateIsSame()) {
                addSessionData(req, capacity);
                req.getSession().setAttribute("sameCoordinates", "Can't be same address coordinates");
                resp.sendRedirect("/create_order");
                return;
            }

            CategoryService categoryService = new CategoryService();
            OrderTaxi orderTaxi = new OrderTaxi();
            Account account = (Account) req.getSession().getAttribute("account");
            orderTaxi.setAccountId(account.getId());
            orderTaxi.setPassengerNumber(capacity);
            orderTaxi.setDestinationAddress(req.getParameter("destinationAddress"));
            orderTaxi.setPickupAddress(req.getParameter("pickupAddress"));
            orderTaxi.setLanguageId(language.getId());

            orderTaxi.setLongitudeDestionation(lonD);
            orderTaxi.setLatitudeDestination(latD);
            orderTaxi.setLongitudePickup(lonP);
            orderTaxi.setLatitudePickup(latP);

            BigDecimal costByKm = categoryService.getCostByKm(language.getId(),
                    req.getParameter("category"));
            int km = Distance.getDistanceFromLatLonInKm(latD.doubleValue(), lonD.doubleValue(), latP.doubleValue(), lonP.doubleValue());
            Loyalty loyalty = accountService.getAccountLoyalty(account.getId());
            BigDecimal cost;
            if (loyalty != null && loyalty.getDiscount() > 0) {
                cost = calculateLoyaltyCost(km, loyalty.getDiscount(), costByKm);
            } else {
                cost = calculateCost(km, costByKm);
            }
            cost = cost.multiply(new BigDecimal(cars.size()));
            orderTaxi.setCost(cost);
            orderService.createOrder(orderTaxi);

            cars.forEach(car -> carService.updateCarStatus(car.getId(), Status.BUSY));

            LOGGER.info("Order complete with cost: " + cost);

            String carsNames = cars.stream()
                                    .map(Car::getModelName)
                                    .collect(Collectors.joining(", "));
            String costFormatted = CurrencyLang.formatCurrency(lang, cost);
            ParkAddressService parkAddressService = new ParkAddressService();
            CarParkAddress parkAddress = parkAddressService.getCarParkAddress();
            int waitKm = Distance.getDistanceFromLatLonInKm(latP.doubleValue(),
                    lonP.doubleValue(),
                    parkAddress.getLatitude().doubleValue(),
                    parkAddress.getLongitude().doubleValue());

            int minutes = minutesSpend(waitKm);

            req.getSession().setAttribute("orderCompleted",
                    "Order Completed. You cars are " + carsNames + ". " +
                            "Wait it for " + minutes + " minutes over " + orderTaxi.getPickupAddress()  + ". \n" +
                            "Cost is " + costFormatted);
        } else {
            LOGGER.info("Order several cars has been failed.");

            req.getSession().setAttribute("orderFailed",
                    "Sorry. There are no cars available according to your settings.");
        }
        LOGGER.info("Redirect to the order status.");
        resp.sendRedirect("/order_status");
    }

    private static BigDecimal calculateCost(int km, BigDecimal costByKm) {
        return costByKm.multiply(new BigDecimal(km));
    }

    private static BigDecimal calculateLoyaltyCost(int km, int loyalty, BigDecimal costByKm) {
        BigDecimal result = costByKm.multiply(new BigDecimal(km));
        BigDecimal left = result.multiply(new BigDecimal(loyalty))
                .divide(new BigDecimal(100));
        return result.subtract(left);
    }

    private void addSessionData(HttpServletRequest req, int capacity) {
        req.getSession().setAttribute("capacity", capacity);
        req.getSession().setAttribute("destinationAddress",
                req.getParameter("destinationAddress"));
        req.getSession().setAttribute("pickupAddress",
                req.getParameter("pickupAddress"));
        req.getSession().setAttribute("category",
                req.getParameter("category"));

        req.getSession().setAttribute("longitude_destination",
                req.getParameter("longitude_destination"));
        req.getSession().setAttribute("latitude_destination",
                req.getParameter("latitude_destination"));
        req.getSession().setAttribute("longitude_pickup",
                req.getParameter("longitude_pickup"));
        req.getSession().setAttribute("latitude_pickup",
                req.getParameter("latitude_pickup"));

    }
    private void clearOrderSessionData(HttpServletRequest req) {
        req.getSession().setAttribute("orderFailed", null);
        req.getSession().setAttribute("orderCompleted", null);
        req.getSession().setAttribute("findAnotherOrder", null);
        req.getSession().setAttribute("capacity", null);
        req.getSession().setAttribute("destinationAddress", null);
        req.getSession().setAttribute("pickupAddress", null);

        req.getSession().setAttribute("longitude_destination", null);
        req.getSession().setAttribute("latitude_destination", null);
        req.getSession().setAttribute("longitude_pickup", null);
        req.getSession().setAttribute("latitude_pickup", null);

        req.getSession().setAttribute("sameCoordinates", null);
        req.getSession().setAttribute("coordinatesOutOfRange", null);
    }
}
