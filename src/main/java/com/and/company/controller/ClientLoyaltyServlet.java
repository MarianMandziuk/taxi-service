package com.and.company.controller;

import com.and.company.entity.Account;
import com.and.company.entity.Loyalty;
import com.and.company.service.AccountService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/client_loyalty")
public class ClientLoyaltyServlet extends HomeServlet {
    private final Logger LOGGER =  Logger.getLogger(ClientOrdersServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("Client loyalty method get has been called.");

        Account account = (Account) req.getSession().getAttribute("account");
        String accountEmail = account.getEmail();
        AccountService accountService = new AccountService();
        account = accountService.getAccount(accountEmail);
        req.getSession().setAttribute("account", account);
        Loyalty loyalty = accountService.getAccountLoyalty(account.getLoyaltyId());
        req.setAttribute("loyalty", loyalty);
        req.getRequestDispatcher("views/client_loyalty.jsp").forward(req, resp);
    }
}
