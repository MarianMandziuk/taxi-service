package com.and.company.controller.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(urlPatterns = {"/*"},
        initParams = {@WebInitParam(name = "lang", value = "ua"),
                      @WebInitParam(name = "bundle", value = "resources")})
public class InternationalizeFilter implements Filter {
    private final Logger LOGGER =  Logger.getLogger(InternationalizeFilter.class);
    private static final String LANG = "lang";
    private static final String BUNDLE = "bundle";
    private String defaultBundle;
    private String lang;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        defaultBundle = filterConfig.getInitParameter(BUNDLE);
        lang = filterConfig.getInitParameter(LANG);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String localeParameter = request.getParameter(LANG);
        LOGGER.info("Set lang Filter.");
        lang = localeParameter != null ? localeParameter : httpRequest.getSession().getAttribute(LANG) != null
                ? (String) httpRequest.getSession().getAttribute(LANG) : this.lang;

        httpRequest.getSession().setAttribute(LANG, lang);
        httpRequest.getSession().setAttribute(BUNDLE, defaultBundle);
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
