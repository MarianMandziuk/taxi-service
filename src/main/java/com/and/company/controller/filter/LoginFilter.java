package com.and.company.controller.filter;

import com.and.company.entity.Account;
import com.and.company.entity.Role;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebFilter("/*")
public class LoginFilter implements Filter {

    private final Logger LOGGER =  Logger.getLogger(LoginFilter.class);
    List<String> customerURL;
    List<String> adminURL;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.info("Init filter");
        customerURL = new ArrayList<>();
        customerURL.add("/create_order");
        customerURL.add("/order_status");
        customerURL.add("/client_orders");
        customerURL.add("/client_loyalty");

        adminURL = new ArrayList<>();
        adminURL.add("/orders");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        LOGGER.info("Login filter.");
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(true);

        String path = request.getServletPath();
        LOGGER.info("Login filter proccess path: " + path);

        if ((session.getAttribute("account") != null) && !path.equals("/logout")) {
            LOGGER.info("User is logined");
            Account account = (Account) request.getSession().getAttribute("account");
            long roleId = account.getRoleId();
            if (roleId == Role.CUSTOMER.getRoleId() && customerURL.contains(path)) {
                LOGGER.info("Role - Customer, go to customer's page");
                filterChain.doFilter(request, response);
            } else if (roleId == Role.ADMIN.getRoleId() && adminURL.contains(path)) {
                LOGGER.info("Role - Admin, go to admin page");
                filterChain.doFilter(request, response);
            } else if (roleId == Role.CUSTOMER.getRoleId()) {
                LOGGER.info("Account : " + account.getEmail() + " forwared to /create_order");
                request.getRequestDispatcher("/create_order").forward(request, response);
            } else if (roleId == Role.ADMIN.getRoleId()) {
                LOGGER.info("Account : " + account.getEmail() + " forwared to /admin");
                request.getRequestDispatcher("/orders").forward(request, response);
            }
        } else {
            if (path.equals("/login")
                || path.equals("/signup")
                || path.equals("/logout")) {
                LOGGER.info("Go to the path: " + path);
                filterChain.doFilter(request, response);
            } else {
                path = "/";
                LOGGER.info("Go to the path: " + path);
                request.getRequestDispatcher(path).forward(request, response);
            }
        }
    }

    @Override
    public void destroy() {

    }
}