package com.and.company.db;

import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DBManager {
	
	private static final Logger LOGGER = Logger.getLogger(DBManager.class);

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null)
			instance = new DBManager();
		return instance;
	}

	public Connection getConnection() throws SQLException {
		Connection con = null;
		try {
			Context context = (Context) new InitialContext().lookup("java:/comp/env");
			DataSource dataSource = (DataSource) context.lookup("jdbc/mysql");
			con = dataSource.getConnection();
		} catch (NamingException ex) {
			LOGGER.error("Cannot obtain a connection from the pool", ex);
		}
		return con;
	}

	private DBManager() {
	}

	public void close(AutoCloseable... autoCloseables) {
		for (AutoCloseable ac : autoCloseables) {
			if (ac != null) {
				try {
					ac.close();
				} catch (Exception ex) {
					LOGGER.error("Cannot close autocloseable. ", ex);
					throw new IllegalStateException("Cannot close " + ac);
				}
			}
		}
	}
}