<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
    prefix="fn" %>


<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="${bundle}"/>

<jsp:include page="components/header.jsp"/>
<jsp:include page="components/container_header.jsp"/>

    <c:choose>
        <c:when test="$not empty loyalty}">
            You loyalty is <c:out value="${loyalty}" /> %.
        </c:when>
        <c:otherwise>
         You have not any loyalty.
        </c:otherwise>
    </c:choose>


<jsp:include page="components/container_footer.jsp"/>
<jsp:include page="components/footer.jsp"/>