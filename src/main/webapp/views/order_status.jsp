<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="${bundle}"/>

<jsp:include page="components/header.jsp"/>
<jsp:include page="components/container_header.jsp"/>

    <c:if test="${not empty orderCompleted}">
             <c:out value="${orderCompleted}"/>
    </c:if>
    <c:if test="${not empty orderFailed}">
             <c:out value="${orderFailed}"/>
    </c:if>
<jsp:include page="components/container_footer.jsp"/>
<jsp:include page="components/footer.jsp"/>