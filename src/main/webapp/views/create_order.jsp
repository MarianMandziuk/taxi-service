<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="${bundle}"/>

<jsp:include page="components/header.jsp"/>

 <div class="jumbotron">
    <div class="container">

    <c:if test="${not empty findAnotherOrder}">
        <c:out value="${findAnotherOrder}"/>
    </c:if>

    <div class="row">
        <div class="col-6 ">
            <form method="post" action="${pageContext.request.contextPath}/create_order">
              <div class="form-group">
                <label for="capacity"><fmt:message key="create_order.capacity"/></label>
                <input type="number" class="form-control" id="capacity" aria-describedby="emailHelp" name="capacity" min="0"
                    value="<c:if test="${not empty capacity}"><c:out value="${capacity}"/></c:if>">
              </div>


              <div class="p-1 shadow p-3 mb-5 rounded">
                  <div class="form-group">
                    <label for="pickupAddress"><fmt:message key="create_order.pickup_address"/></label>
                    <input type="text" class="form-control" id="pickupAddress" name="pickupAddress"
                        value="<c:if test="${not empty pickupAddress}"><c:out value="${pickupAddress}"/></c:if>">
                  </div>
                     <div class="form-group">
                        <label for="latitude_pickup"><fmt:message key="create_order.latitude_pickup"/></label>
                        <input type="number" class="form-control" id="latitude_pickup" name="latitude_pickup" step="0.000001"
                            value="<c:if test="${not empty latitude_pickup}"><c:out value="${latitude_pickup}"/></c:if>">
                     </div>
                    <div class="form-group">
                      <label for="longitude_pickup"><fmt:message key="create_order.longitude_pickup"/></label>
                      <input type="number" class="form-control" id="longitude_pickup" name="longitude_pickup" step="0.000001"
                         value="<c:if test="${not empty longitude_pickup}"><c:out value="${longitude_pickup}"/></c:if>">
                    </div>
                </div>

              <div class="p-1 shadow p-3 mb-5 rounded">
                  <div class="form-group">
                    <label for="destinationAddress"><fmt:message key="create_order.destination_address"/></label>
                    <input type="text" class="form-control" id="destinationAddress" name="destinationAddress"
                        value="<c:if test="${not empty destinationAddress}"><c:out value="${destinationAddress}"/></c:if>">
                  </div>
                   <div class="form-group">
                      <label for="latitude_destination"><fmt:message key="create_order.latitude_destination"/></label>
                      <input type="number" class="form-control" id="latitude_destination" name="latitude_destination" step="0.000001"
                          value="<c:if test="${not empty latitude_destination}"><c:out value="${latitude_destination}"/></c:if>">
                   </div>
                  <div class="form-group">
                    <label for="longitude_destination"><fmt:message key="create_order.longitude_destination"/></label>
                    <input type="number" class="form-control" id="longitude_destination" name="longitude_destination" step="0.000001"
                       value="<c:if test="${not empty longitude_destination}"><c:out value="${longitude_destination}"/></c:if>">
                  </div>
              </div>

              <div class="text-danger">
                <c:if test="${not empty coordinatesOutOfRange}">
                   <fmt:message key="create_order.error.coordinates.range"/>
                </c:if>
              </div>
              <div class="text-danger">
                  <c:if test="${not empty sameCoordinates}">
                     <fmt:message key="create_order.error.coordinates.same"/>
                  </c:if>
                </div>
                <div class="text-danger">
                  <c:if test="${not empty incorrectData}">
                     <fmt:message key="error.incorrect.data"/>
                  </c:if>
                </div>

              <div class="form-group">
                 <div class="form-group">
                     <label for="carCategory"><fmt:message key="create_order.car_category"/></label>
                     <select class="form-control" id="carCategory" name="category">
                        <c:forEach var="categ" items="${categories}">
                            <option><c:out value="${categ.description}" /></option>
                        </c:forEach>
                     </select>
                   </div>
              </div>
              <button type="submit" class="btn btn-primary"><fmt:message key="create_order.create"/></button>
                  <c:if test="${not empty findAnotherOrder}">
                      <button name="orderSeveralCar" type="submit" class="btn btn-primary"><fmt:message key="create_order.create.several.car"/></button>
                      <a name="cancel" href="/client_orders" type="submit" class="btn btn-secondary"><fmt:message key="create_order.cancel"/></a>
                  </c:if>
            </form>


        </div>
        <div class="col-6">
            <div style="padding-top: 100px;">
            <div id="mapid"></div>
             <div class="row m-2">
                    <div class="col-6 text-center">
                         <button class="btn btn-primary btn-sm active" role="button" aria-pressed="true"
                         onclick="setPickupCoordinates();" id="set_pickup_coordinates"
                         ><fmt:message key="create_order.button.pickup"/></button>
                    </div>
                    <div class="col-6 text-center">
                         <button href="#" class="btn btn-primary btn-sm active" role="button" aria-pressed="true"
                         onclick="setDestinationCoordinates();" id="set_destination_coordinates"
                         ><fmt:message key="create_order.button.destination"/></button>
                    </div>

             </div>
             </div>
        </div>

        </div>
    </div>
</div>



<script>
var mymap = L.map('mapid').setView([50.447374,30.528421], 10);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);
var mmr = L.marker([0,0]);
mmr.bindPopup('0,0');
mmr.addTo(mymap);

var currentLat = 50.447374;
var currentLng = 30.528421;
var currentAddress = "";

mymap.on('click', onMapClick);

function onMapClick(e) {
    mmr.setLatLng(e.latlng);
    currentLat = Number(e.latlng.lat).toFixed(6);
    currentLng = Number(e.latlng.lng).toFixed(6);
    var geocodeService = L.esri.Geocoding.geocodeService();
    <c:choose>
       <c:when test="${lang == 'ua'}">
           geocodeService.reverse().language("ua").latlng(e.latlng).run(function (error, result) {
                 if (error) {
                   return;
                 }

                 currentAddress = result.address.LongLabel;
               });
       </c:when>
       <c:otherwise>
            geocodeService.reverse().language("en").latlng(e.latlng).run(function (error, result) {
                 if (error) {
                   return;
                 }

                 currentAddress = result.address.LongLabel;
               });
       </c:otherwise>
     </c:choose>
}

function setui(lt,ln,zm) {
    lt = Number(lt).toFixed(6);
    ln = Number(ln).toFixed(6);
}

function setui(lt,ln,zm) {
    lt = Number(lt).toFixed(6);
    ln = Number(ln).toFixed(6);
    document.getElementById("latitude").value=lt;
    document.getElementById("lng").value=ln;

}

function setDestinationCoordinates() {
    document.getElementById("latitude_destination").value = currentLat;
    document.getElementById("longitude_destination").value = currentLng;
    document.getElementById("destinationAddress").value = currentAddress;
}

function setPickupCoordinates() {
    document.getElementById("latitude_pickup").value=currentLat;
    document.getElementById("longitude_pickup").value=currentLng;
    document.getElementById("pickupAddress").value = currentAddress;
}






</script>



<jsp:include page="components/footer.jsp"/>
