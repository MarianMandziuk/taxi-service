<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
    prefix="fn" %>

<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="${bundle}"/>

<jsp:include page="components/header.jsp"/>
 <div class="jumbotron p-4">
    <div class="container">




 <div class="row m-2">
    <div class="col-6 p-1 shadow p-3 mb-5 rounded">
        <div class="row mb-1">
            <div class="col-3">
                from :
            </div>
            <div class="col-9">
                <input id="date1" type="date">
            </div>
        </div>
        <div class="row mb-1">
            <div class="col-3">
                to :
            </div>
            <div class="col-9">
                <input id="date2" type="date">
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-12 text-center">
                 <a href="#" class="btn btn-primary btn-sm active" role="button" aria-pressed="true"
                 onclick="createDatesUrl();" id="send_by_dates_link"
                 >Filter by dates</a>
            </div>
        </div>


    </div>
    <div class="col-6 p-1 shadow p-3 mb-5 rounded">
       <div class="row mb-1">
            <div class="col-3">
                Name :
            </div>
            <div class="col-9">
                <input type="text" class="form-control" placeholder="Username" aria-label="Username"
                 aria-describedby="addon-wrapping" id="clientname">
            </div>
       </div>
       <div class="row mb-1 ">
            <div class="col-12 text-center">
                 <a href="#" class="btn btn-primary btn-sm active" role="button" aria-pressed="true"
                 onclick="createClientNameUrl()" id="send_by_client_name_link">Filter by name</a>
            </div>
       </div>
    </div>
 </div>
<c:out value="${date1}" />




    <c:choose>
    <c:when test="${fn:length(orders) == 0}">No such orders</c:when>

    <c:otherwise>
    <table class="table table-sm">
     <thead>
        <tr>
          <th scope="col">
                <a href="/orders">
                   №
                </a></th>
          <th scope="col"><fmt:message key="order.username"/></th>
          <th scope="col"><fmt:message key="order.passengernumber"/></th>
          <th scope="col"><fmt:message key="order.destinationaddress"/></th>
          <th scope="col"><fmt:message key="order.pickupaddress"/></th>
          <th scope="col">
               <c:choose>
               <c:when test="${isDesc == true}">
                   <a href="/orders/?cost=true&isDesc=false">
                       <fmt:message key="order.cost"/>
                   </a>
               </c:when>
               <c:otherwise>
                  <a href="/orders/?cost=true&isDesc=true">
                      <fmt:message key="order.cost"/>
                  </a>
               </c:otherwise>
               </c:choose>
          </th>
          <th scope="col">
               <c:choose>
               <c:when test="${isDesc == true}">
                   <a href="/orders/?create_time=true&isDesc=false">
                       <fmt:message key="order.createtime"/>
                   </a>
               </c:when>
               <c:otherwise>
                  <a href="/orders/?create_time=true&isDesc=true">
                      <fmt:message key="order.createtime"/>
                  </a>
               </c:otherwise>
               </c:choose>
          </th>
          <th scope="col"><fmt:message key="order.language"/></th>
        </tr>
      </thead>
        <c:forEach var="order" items="${orders}" varStatus="loop">
            <tr>
                <td>${loop.index + from}</td>
                <td>${order.userName}</td>
                <td>${order.passengerNumber}</td>
                <td>${order.destinationAddress}</td>
                <td>${order.pickupAddress}</td>
                <td>
                    <c:choose>
                        <c:when test="${order.language == 'ua'}">
                            <fmt:formatNumber type="currency" value="${order.cost}" currencySymbol="₴"/>
                        </c:when>

                        <c:otherwise>
                            <fmt:formatNumber type="currency" value="${order.cost}" currencySymbol="$"/>
                        </c:otherwise>
                    </c:choose>

                </td>
                <td>${order.createTime}</td>
                <td>${order.language}</td>
            </tr>
        </c:forEach>
    </table>
    </c:otherwise>
    </c:choose>

    <nav aria-label="pagination orders">
      <ul class="pagination">
      <%--===========================================================================
                  PRESIOUS BEGIN.
      ===========================================================================--%>
       <c:choose>
            <%--===========================================================================
            Default sorting.
            ===========================================================================--%>
           <c:when test="${from <= 1}">
               <li class="page-item disabled">
                 <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
               </li>
           </c:when>
           <%--===========================================================================
            Default sorting.
           ===========================================================================--%>
       <c:otherwise>
               <c:choose>
               <%--===========================================================================
                           Create time sorting.
               ===========================================================================--%>
                   <c:when test="${not empty create_time}">
                         <c:choose>
                           <c:when test="${isDesc == 'true'}">
                               <c:set var="dynamicURL" value="/order/?from=${from - number}&create_time=true&isDesc=true"/>
                           </c:when>
                           <c:otherwise>
                               <c:set var="dynamicURL" value="/order/?from=${from - number}&create_time=true&isDesc=false"/>
                           </c:otherwise>
                         </c:choose>
                   </c:when>
               <%--===========================================================================
                           Create time sorting end.
               ===========================================================================--%>
               <%--===========================================================================
                           Cost sorting.
               ===========================================================================--%>
                   <c:when test="${not empty cost}">
                         <c:choose>
                           <c:when test="${isDesc == 'true'}">
                               <c:set var="dynamicURL" value="/order/?from=${from - number}&cost=true&isDesc=true"/>
                           </c:when>
                           <c:otherwise>
                               <c:set var="dynamicURL" value="/order/?from=${from - number}&cost=true&isDesc=false"/>
                           </c:otherwise>
                         </c:choose>
                   </c:when>
               <%--===========================================================================
                          Cost sorting end.
               ===========================================================================--%>

               <%--===========================================================================
                           Filter by dates.
               ===========================================================================--%>
                   <c:when test="${not empty filter_date}">
                       <c:set var="dynamicURL"
                       value="/order/?from=${from - number}&filter_date=true&from_date=${from_date}&to_date=${to_date}" />
                   </c:when>
               <%--===========================================================================
                          Filter by dates end.
               ===========================================================================--%>

              <%--===========================================================================
                          Filter by name.
              ===========================================================================--%>
                  <c:when test="${not empty clientname}">
                      <c:set var="dynamicURL"
                      value="/order/?from=${from - number}&clientname=${clientname}" />
                  </c:when>
              <%--===========================================================================
                         Filter by name end.
              ===========================================================================--%>
                   <c:otherwise>
                      <c:set var="dynamicURL" value="/orders/?from=${from - number}"/>
                   </c:otherwise>
               </c:choose>
               <li class="page-item">
                    <a class="page-link" href="${dynamicURL}" tabindex="-1" aria-disabled="true">Previous</a>
                </li>
       </c:otherwise>
       </c:choose>
<%--===========================================================================
            PREVIOUS END.
===========================================================================--%>
<%--===========================================================================
            NEXT BEGIN.
===========================================================================--%>
       <c:choose>
           <c:when test="${fn:length(orders) < number}">
                   <li class="page-item disabled">
                     <a class="page-link" href="#">Next</a>
                   </li>
           </c:when>
       <c:otherwise>
            <c:choose>
            <%--===========================================================================
                        Create time sorting 2 begin.
             ===========================================================================--%>
               <c:when test="${not empty create_time}">
                     <c:choose>
                       <c:when test="${isDesc == 'true'}">
                           <c:set var="dynamicURL" value="/order/?from=${from + number}&create_time=true&isDesc=true"/>
                       </c:when>
                       <c:otherwise>
                           <c:set var="dynamicURL" value="/order/?from=${from + number}&create_time=true&isDesc=false"/>
                       </c:otherwise>
                     </c:choose>
               </c:when>
           <%--===========================================================================
                      Create time sorting 2 sorting end.
           ===========================================================================--%>
            <%--===========================================================================
                        Cost sorting 2 begin.
             ===========================================================================--%>
               <c:when test="${not empty cost}">
                     <c:choose>
                       <c:when test="${isDesc == 'true'}">
                           <c:set var="dynamicURL" value="/order/?from=${from + number}&cost=true&isDesc=true"/>
                       </c:when>
                       <c:otherwise>
                           <c:set var="dynamicURL" value="/order/?from=${from + number}&cost=true&isDesc=false"/>
                       </c:otherwise>
                     </c:choose>
               </c:when>
           <%--===========================================================================
                      Cost sorting 2 sorting end.
           ===========================================================================--%>
           <%--===========================================================================
                       Filter by dates 2.
           ===========================================================================--%>
               <c:when test="${not empty filter_date}">
                   <c:set var="dynamicURL"
                   value="/order/?from=${from + number}&filter_date=true&from_date=${from_date}&to_date=${to_date}" />
               </c:when>
           <%--===========================================================================
                      Filter by dates end 2.
           ===========================================================================--%>
           <%--===========================================================================
                       Filter by name 2.
           ===========================================================================--%>
               <c:when test="${not empty clientname}">
                   <c:set var="dynamicURL"
                   value="/order/?from=${from + number}&clientname=${clientname}" />
               </c:when>
           <%--===========================================================================
                      Filter by name end 2.
           ===========================================================================--%>
               <c:otherwise>
                  <c:set var="dynamicURL" value="/orders/?from=${from + number}"/>
               </c:otherwise>
           </c:choose>
        <li class="page-item">
          <a class="page-link" href="${dynamicURL}">Next</a>
        </li>
       </c:otherwise>
       </c:choose>
<%--===========================================================================
            NEXT END.
===========================================================================--%>

      </ul>
    </nav>

    </div>
    </div>


<script>
    date1.max = new Date().toISOString().split("T")[0];
    date2.max = new Date().toISOString().split("T")[0];
    <c:choose>
       <c:when test="${not empty from_date}">
           date1.value = "${from_date}";
       </c:when>
       <c:otherwise>
            date1.value = new Date().toISOString().split("T")[0];
       </c:otherwise>
     </c:choose>
    <c:choose>
       <c:when test="${not empty to_date}">
           date2.value = "${to_date}";
       </c:when>
       <c:otherwise>
            date2.value = new Date().toISOString().split("T")[0];
       </c:otherwise>
     </c:choose>

    function createDatesUrl() {
        var date1 = document.getElementById("date1").value;
        var date2 = document.getElementById("date2").value;
        var link = document.getElementById("send_by_dates_link");
            link.setAttribute("href", "/orders/?filter_date=true&from_date=" + date1 + "&to_date=" +date2);
    }

    function createClientNameUrl() {
        var clientname = document.getElementById("clientname").value;
        var link = document.getElementById("send_by_client_name_link");
            link.setAttribute("href", "/orders/?clientname=" + clientname);
    }
</script>
<jsp:include page="components/footer.jsp"/>