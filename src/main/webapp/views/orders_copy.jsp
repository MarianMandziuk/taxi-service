<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
    prefix="fn" %>

<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="${bundle}"/>

<jsp:include page="components/header.jsp"/>
 <div class="jumbotron">
    <div class="container">


    <c:choose>
    <c:when test="${fn:length(orders) == 0}">No such orders</c:when>

    <c:otherwise>
    <table class="table table-sm">
     <thead>
        <tr>
          <th scope="col">№</th>
          <th scope="col"><fmt:message key="order.username"/></th>
          <th scope="col"><fmt:message key="order.passengernumber"/></th>
          <th scope="col"><fmt:message key="order.destinationaddress"/></th>
          <th scope="col"><fmt:message key="order.pickupaddress"/></th>
          <th scope="col"><fmt:message key="order.cost"/></th>
          <th scope="col">
               <c:choose>
               <c:when test="${isDesc == true}">
                   <a href="/orders/?create_time=true&isDesc=false">
                       <fmt:message key="order.createtime"/>
                   </a>
               </c:when>
               <c:otherwise>
                  <a href="/orders/?create_time=true&isDesc=true">
                      <fmt:message key="order.createtime"/>
                  </a>
               </c:otherwise>
               </c:choose>
          </th>
          <th scope="col"><fmt:message key="order.language"/></th>
        </tr>
      </thead>
        <c:forEach var="order" items="${orders}" varStatus="loop">
            <tr>
                <td>${loop.index + from}</td>
                <td>${order.userName}</td>
                <td>${order.passengerNumber}</td>
                <td>${order.destinationAddress}</td>
                <td>${order.pickupAddress}</td>
                <td>
                    <c:choose>
                        <c:when test="${order.language == 'ua'}">
                            <fmt:formatNumber type="currency" value="${order.cost}" currencySymbol="₴"/>
                        </c:when>

                        <c:otherwise>
                            <fmt:formatNumber type="currency" value="${order.cost}" currencySymbol="$"/>
                        </c:otherwise>
                    </c:choose>

                </td>
                <td>${order.createTime}</td>
                <td>${order.language}</td>
            </tr>
        </c:forEach>
    </table>
    </c:otherwise>
    </c:choose>

    <nav aria-label="pagination orders">
      <ul class="pagination">
       <c:choose>
           <c:when test="${from <= 1}">
               <li class="page-item disabled">
                 <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
               </li>
           </c:when>
       <c:otherwise>
               <li class="page-item">
                 <a class="page-link" href="/orders/?from=${from - number}" tabindex="-1" aria-disabled="true">Previous</a>
               </li>
       </c:otherwise>
       </c:choose>

       <c:choose>
       <c:when test="${fn:length(orders) < number}">
               <li class="page-item disabled">
                 <a class="page-link" href="#">Next</a>
               </li>
       </c:when>
       <c:otherwise>
        <li class="page-item">
          <a class="page-link" href="/orders/?from=${from + number}">Next</a>
        </li>
       </c:otherwise>
       </c:choose>


      </ul>
    </nav>

    </div>
    </div>





    <c:choose>
        <c:when test="${not empty create_time}">
              <c:choose>
                <c:when test="${isDesc == 'true'}">
                    <c:set var="dynamicURL" value="/order/from?=${from + number}?create_time=true&isDesc=true"/>
                </c:when>
                <c:otherwise>
                    <c:set var="dynamicURL" value="/order/from?=${from + number}?create_time=true&isDesc=false"/>
                </c:otherwise>
              </c:choose>
        </c:when>
        <c:otherwise>
            other
        </c:otherwise>
    </c:choose>
    ${dynamicURL}

<jsp:include page="components/footer.jsp"/>