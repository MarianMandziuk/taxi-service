<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="${bundle}"/>

<jsp:include page="components/header.jsp"/>
<jsp:include page="components/container_header.jsp"/>
 <c:choose>
   <c:when test="${not empty newUserName}">
      <c:out value="${newUserName}" />
      <fmt:message key="signup.welcome.message"/>
       <a href="/login"> --></a>
   </c:when>
   <c:otherwise>

<form method="post" action="${pageContext.request.contextPath}/signup">
  <div class="form-group">
    <label for="exampleInputEmail1"><fmt:message key="signup.email"/></label>
    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1"><fmt:message key="signup.password"/></label>
    <input name="password" type="password" class="form-control" id="exampleInputPassword1">
  </div>
  <div class="form-group">
    <label for="password_repeat"><fmt:message key="signup.password"/></label>
    <input type="password" class="form-control" id="password_repeat" name="password_repeat">
  </div>
    <div class="text-danger">
      <c:if test="${not empty errorPasswordRepeat}">
         <fmt:message key="login.error.message.password"/>
      </c:if>
    </div>
  <div class="form-group">
    <label for="name"><fmt:message key="signup.name"/></label>
    <input class="form-control" type="text"  name="name">
  </div>
  <div class="text-danger">
      <c:if test="${not empty errorMessageSignup}">
         <fmt:message key="signup.error.message"/>
      </c:if>
    </div>
  <button type="submit" class="btn btn-primary"><fmt:message key="signup.create"/></button>
</form>

   </c:otherwise>
 </c:choose>

<jsp:include page="components/container_footer.jsp"/>
<jsp:include page="components/footer.jsp"/>