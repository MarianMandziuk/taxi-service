<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="${bundle}"/>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.80.0">
    <title>Jumbotron Template · Bootstrap v4.6</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

 <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
   integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
   crossorigin=""/>

    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
      integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
      crossorigin=""></script>


 <script src="https://unpkg.com/esri-leaflet@2.5.3/dist/esri-leaflet.js"
    integrity="sha512-K0Vddb4QdnVOAuPJBHkgrua+/A9Moyv8AQEWi0xndQ+fqbRfAFd47z4A9u1AW/spLO0gEaiE1z98PK1gl5mC5Q=="
    crossorigin=""></script>
      <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
          integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
          crossorigin="">
        <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
          integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
          crossorigin=""></script>


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      .dropdown-menu {
          min-width: 3rem;
      }


      #mapid { height: 280px; }
    </style>

    <!-- Custom styles for this template -->
    <style>
        body {
          padding-top: 3.5rem;
        }
    </style>
  </head>
  <body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" style="color: #faaf3e;"><fmt:message key="header.name"/></a>
      <div class="collapse navbar-collapse text-nowrap" id="navbarCollapse">
           <c:if test="${sessionScope.account.roleId == '2'}">
            <ul class="navbar-nav">
              <li class="nav-item">
                  <c:choose>
                     <c:when test="${requestScope['javax.servlet.forward.servlet_path'] == '/create_order'}">
                        <a class="nav-link active" href="/create_order"><fmt:message key="order.create"/></a>
                     </c:when>
                     <c:otherwise>
                         <a class="nav-link" href="/create_order"><fmt:message key="order.create"/></a>
                     </c:otherwise>
                 </c:choose>
              </li>
              <li class="nav-item">
                  <c:choose>
                     <c:when test="${requestScope['javax.servlet.forward.servlet_path'] == '/client_orders'}">
                        <a class="nav-link active" href="/client_orders"><fmt:message key="client.order"/></a>
                     </c:when>
                     <c:otherwise>
                        <a class="nav-link" href="/client_orders"><fmt:message key="client.order"/></a>
                     </c:otherwise>
                 </c:choose>
                </li>
            <li class="nav-item">
                  <c:choose>
                     <c:when test="${requestScope['javax.servlet.forward.servlet_path'] == '/client_loyalty'}">
                         <a class="nav-link active" href="/client_loyalty"><fmt:message key="client.loyalty"/></a>
                     </c:when>
                     <c:otherwise>
                         <a class="nav-link" href="/client_loyalty"><fmt:message key="client.loyalty"/></a>
                     </c:otherwise>
                 </c:choose>
            </li>
             </ul>
               </c:if>
          </div>

    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <c:choose>
                  <c:when test="${lang=='ua'}">
                      <a style="color: white;" class="nav-link" href="?lang=ua">ua</a>
                  </c:when>
                  <c:otherwise>
                      <a class="nav-link" href="?lang=ua">ua</a>
                  </c:otherwise>
              </c:choose>
            </li>
            <li class="nav-item">
                <c:choose>
                  <c:when test="${lang=='en'}">
                      <a style="color: white;" class="nav-link" href="?lang=en">en</a>
                  </c:when>
                  <c:otherwise>
                      <a class="nav-link" href="?lang=en">en</a>
                  </c:otherwise>
                </c:choose>
            </li>
            <li class="nav-item active">
                <c:if test="${not empty sessionScope.account.name}">
                 <div class="dropdown d-inline-block">
                      <a class="nav-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <c:out value="${sessionScope.account.name}"/>
                      </a>
                      <div class="dropdown-menu dropdown-menu-lg-right" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="/logout">Exit</a>
                      </div>
                   </div>
                </c:if>
            </li>
        </ul>
    </div>
</nav>


