<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"
    prefix="fn" %>


<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="${bundle}"/>

<jsp:include page="components/header.jsp"/>
<jsp:include page="components/container_header.jsp"/>

    <c:choose>
        <c:when test="${fn:length(orders) == 0}">No such orders</c:when>

        <c:otherwise>
        <table class="table table-sm">
         <thead>
            <tr>
              <th scope="col"></th>
              <th scope="col"><fmt:message key="order.destinationaddress"/></th>
              <th scope="col"><fmt:message key="order.pickupaddress"/></th>
              <th scope="col"><fmt:message key="order.cost"/></th>
              <th scope="col"><fmt:message key="order.passengernumber"/></th>
            </tr>
          </thead>
            <c:forEach var="order" items="${orders}" varStatus="loop">
                <tr>
                    <td>${loop.index + from}</td>
                    <td>${order.destinationAddress}</td>
                    <td>${order.pickupAddress}</td>
                    <td>
                        <c:choose>
                            <c:when test="${order.languageId == '2'}">
                                <fmt:formatNumber type="currency" value="${order.cost}" currencySymbol="₴"/>
                            </c:when>

                            <c:otherwise>
                                <fmt:formatNumber type="currency" value="${order.cost}" currencySymbol="$"/>
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>${order.passengerNumber}</td>
                </tr>
            </c:forEach>
        </table>
        </c:otherwise>
        </c:choose>


<jsp:include page="components/container_footer.jsp"/>
<jsp:include page="components/footer.jsp"/>