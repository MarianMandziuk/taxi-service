<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="${bundle}"/>
<jsp:include page="components/header.jsp"/>
<jsp:include page="components/container_header.jsp"/>

<form method="post" action="${pageContext.request.contextPath}/login">
  <div class="form-group">
    <label for="exampleInputEmail1"><fmt:message key="login.email"/></label>
    <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>
   <div class="text-danger">
      <c:if test="${not empty errorMessageEmail}">
         <fmt:message key="login.error.message.email"/>
      </c:if>
    </div>
  <div class="form-group">
    <label for="exampleInputPassword1"><fmt:message key="login.password"/></label>
    <input name="password" type="password" class="form-control" id="exampleInputPassword1">
  </div>
  <div class="text-danger">
    <c:if test="${not empty errorMessagePassword}">
       <fmt:message key="login.error.message.password"/>
    </c:if>
  </div>

  <button type="submit" class="btn btn-primary"><fmt:message key="login.enter"/></button>
</form>

<jsp:include page="components/container_footer.jsp"/>
<jsp:include page="components/footer.jsp"/>