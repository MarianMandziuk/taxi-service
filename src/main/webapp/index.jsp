<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="views/components/header.jsp"/>
<fmt:setLocale value="${lang}"/>
<fmt:setBundle basename="${bundle}"/>

<main role="main">
  <!-- Main jumbotron for a primary marketing message or call to action -->
  <div class="jumbotron">
    <div class="container text-center">
      <h3 class="display-8"><fmt:message key="main.greetings"/></h3>
        <a href=<c:url value="login"/> id="login"><fmt:message key="main.login"/></a>
      <br>
      <fmt:message key="main.or"/>
      <br>
        <a href=<c:url value="signup"/> id="sign"><fmt:message key="main.signup"/></a>

    </div>
  </div>

    <hr>

  </div> <!-- /container -->

</main>
<jsp:include page="views/components/footer.jsp"/>