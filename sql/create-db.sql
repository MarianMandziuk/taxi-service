-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema TaxiService_in
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `TaxiService_in` ;

-- -----------------------------------------------------
-- Schema TaxiService_in
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `TaxiService_in` ;
USE `TaxiService_in` ;

-- -----------------------------------------------------
-- Table `TaxiService_in`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TaxiService_in`.`role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` ENUM("Admin", "Client") NOT NULL DEFAULT 'Client',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TaxiService_in`.`language`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TaxiService_in`.`language` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TaxiService_in`.`loyalty`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TaxiService_in`.`loyalty` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `price` DECIMAL(10,2) UNSIGNED NOT NULL,
  `discount` INT UNSIGNED NOT NULL,
  `language_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_loyalty_translation_language1_idx` (`language_id` ASC) VISIBLE,
  CONSTRAINT `fk_loyalty_translation_language1`
    FOREIGN KEY (`language_id`)
    REFERENCES `TaxiService_in`.`language` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TaxiService_in`.`account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TaxiService_in`.`account` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(32) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` INT NOT NULL DEFAULT 2,
  `loyalty_id` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  INDEX `fk_account_role1_idx` (`role_id` ASC) VISIBLE,
  INDEX `fk_account_loyalty1_idx` (`loyalty_id` ASC) VISIBLE,
  CONSTRAINT `fk_account_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `TaxiService_in`.`role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_account_loyalty1`
    FOREIGN KEY (`loyalty_id`)
    REFERENCES `TaxiService_in`.`loyalty` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TaxiService_in`.`status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TaxiService_in`.`status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` ENUM("available", "inactive", "busy") NOT NULL COMMENT '\n',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TaxiService_in`.`car`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TaxiService_in`.`car` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `model_name` VARCHAR(255) NOT NULL,
  `capacity` INT UNSIGNED NOT NULL,
  `status_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_car_status1_idx` (`status_id` ASC) VISIBLE,
  CONSTRAINT `fk_car_status1`
    FOREIGN KEY (`status_id`)
    REFERENCES `TaxiService_in`.`status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TaxiService_in`.`order_taxi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TaxiService_in`.`order_taxi` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `passenger_number` INT UNSIGNED NOT NULL,
  `destination_address` VARCHAR(255) NOT NULL,
  `pickup_address` VARCHAR(255) NOT NULL,
  `cost` DECIMAL(10,2) UNSIGNED NOT NULL,
  `create_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '\n',
  `account_id` INT NOT NULL,
  `language_id` INT NOT NULL,
  `latitude_destination` DECIMAL(10,6) NOT NULL,
  `longitude_destination` DECIMAL(10,6) NOT NULL,
  `latitude_pickup` DECIMAL(10,6) NOT NULL,
  `longitude_pickup` DECIMAL(10,6) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_order_account1_idx` (`account_id` ASC) VISIBLE,
  INDEX `fk_order_language1_idx` (`language_id` ASC) VISIBLE,
  CONSTRAINT `fk_order_account1`
    FOREIGN KEY (`account_id`)
    REFERENCES `TaxiService_in`.`account` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_language1`
    FOREIGN KEY (`language_id`)
    REFERENCES `TaxiService_in`.`language` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TaxiService_in`.`category_translation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TaxiService_in`.`category_translation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255) NOT NULL,
  `cost_by_km` DECIMAL(5,2) UNSIGNED NOT NULL,
  `language_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_table1_language1_idx` (`language_id` ASC) VISIBLE,
  CONSTRAINT `fk_table1_language1`
    FOREIGN KEY (`language_id`)
    REFERENCES `TaxiService_in`.`language` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TaxiService_in`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TaxiService_in`.`category` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `category_translation_id` INT NOT NULL,
  `car_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_category_category_translation1_idx` (`category_translation_id` ASC) VISIBLE,
  INDEX `fk_category_car1_idx` (`car_id` ASC) VISIBLE,
  CONSTRAINT `fk_category_category_translation1`
    FOREIGN KEY (`category_translation_id`)
    REFERENCES `TaxiService_in`.`category_translation` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_category_car1`
    FOREIGN KEY (`car_id`)
    REFERENCES `TaxiService_in`.`car` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TaxiService_in`.`order_has_car`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TaxiService_in`.`order_has_car` (
  `order_id` INT NOT NULL,
  `car_id` INT NOT NULL,
  PRIMARY KEY (`order_id`, `car_id`),
  INDEX `fk_order_has_car_car1_idx` (`car_id` ASC) VISIBLE,
  INDEX `fk_order_has_car_order1_idx` (`order_id` ASC) VISIBLE,
  CONSTRAINT `fk_order_has_car_order1`
    FOREIGN KEY (`order_id`)
    REFERENCES `TaxiService_in`.`order_taxi` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_has_car_car1`
    FOREIGN KEY (`car_id`)
    REFERENCES `TaxiService_in`.`car` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `TaxiService_in`.`car_park_address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `TaxiService_in`.`car_park_address` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `address` VARCHAR(255) NULL,
  `longitude` DECIMAL(10,6) NOT NULL,
  `latitude` DECIMAL(10,6) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `TaxiService_in`.`role`
-- -----------------------------------------------------
START TRANSACTION;
USE `TaxiService_in`;
INSERT INTO `TaxiService_in`.`role` (`id`, `name`) VALUES (1, 'Admin');
INSERT INTO `TaxiService_in`.`role` (`id`, `name`) VALUES (2, 'Client');

COMMIT;


-- -----------------------------------------------------
-- Data for table `TaxiService_in`.`language`
-- -----------------------------------------------------
START TRANSACTION;
USE `TaxiService_in`;
INSERT INTO `TaxiService_in`.`language` (`id`, `name`) VALUES (1, 'en');
INSERT INTO `TaxiService_in`.`language` (`id`, `name`) VALUES (2, 'ua');

COMMIT;


-- -----------------------------------------------------
-- Data for table `TaxiService_in`.`loyalty`
-- -----------------------------------------------------
START TRANSACTION;
USE `TaxiService_in`;
INSERT INTO `TaxiService_in`.`loyalty` (`id`, `price`, `discount`, `language_id`) VALUES (1, 0, 0, 1);
INSERT INTO `TaxiService_in`.`loyalty` (`id`, `price`, `discount`, `language_id`) VALUES (2, 500, 0.03, 1);
INSERT INTO `TaxiService_in`.`loyalty` (`id`, `price`, `discount`, `language_id`) VALUES (3, 1000, 0.05, 1);
INSERT INTO `TaxiService_in`.`loyalty` (`id`, `price`, `discount`, `language_id`) VALUES (4, 1500, 0.1, 1);
INSERT INTO `TaxiService_in`.`loyalty` (`id`, `price`, `discount`, `language_id`) VALUES (5, 0, 0, 2);
INSERT INTO `TaxiService_in`.`loyalty` (`id`, `price`, `discount`, `language_id`) VALUES (6, 5000, 0.04, 2);
INSERT INTO `TaxiService_in`.`loyalty` (`id`, `price`, `discount`, `language_id`) VALUES (7, 10000, 0.08, 2);
INSERT INTO `TaxiService_in`.`loyalty` (`id`, `price`, `discount`, `language_id`) VALUES (8, 15000, 0.1, 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `TaxiService_in`.`account`
-- -----------------------------------------------------
START TRANSACTION;
USE `TaxiService_in`;
INSERT INTO `TaxiService_in`.`account` (`id`, `email`, `password`, `name`, `create_time`, `role_id`, `loyalty_id`) VALUES (1, 'admin@test.com', '12345', 'admin', now(), 1, NULL);
INSERT INTO `TaxiService_in`.`account` (`id`, `email`, `password`, `name`, `create_time`, `role_id`, `loyalty_id`) VALUES (2, 'test@test.com', '12345', 'test_en', now(), 2, NULL);
INSERT INTO `TaxiService_in`.`account` (`id`, `email`, `password`, `name`, `create_time`, `role_id`, `loyalty_id`) VALUES (3, 'test_ukraine@test.com', '12345', 'тест ', now(), 2, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `TaxiService_in`.`status`
-- -----------------------------------------------------
START TRANSACTION;
USE `TaxiService_in`;
INSERT INTO `TaxiService_in`.`status` (`id`, `description`) VALUES (1, 'available');
INSERT INTO `TaxiService_in`.`status` (`id`, `description`) VALUES (2, 'inactive');
INSERT INTO `TaxiService_in`.`status` (`id`, `description`) VALUES (3, 'busy');

COMMIT;


-- -----------------------------------------------------
-- Data for table `TaxiService_in`.`car`
-- -----------------------------------------------------
START TRANSACTION;
USE `TaxiService_in`;
INSERT INTO `TaxiService_in`.`car` (`id`, `model_name`, `capacity`, `status_id`) VALUES (1, 'volvo s123', 3, 1);
INSERT INTO `TaxiService_in`.`car` (`id`, `model_name`, `capacity`, `status_id`) VALUES (2, 'audi', 2, 1);
INSERT INTO `TaxiService_in`.`car` (`id`, `model_name`, `capacity`, `status_id`) VALUES (3, 'audi 2', 3, 2);
INSERT INTO `TaxiService_in`.`car` (`id`, `model_name`, `capacity`, `status_id`) VALUES (4, 'volkswagen', 5, 3);
INSERT INTO `TaxiService_in`.`car` (`id`, `model_name`, `capacity`, `status_id`) VALUES (5, 'opel', 3, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `TaxiService_in`.`order_taxi`
-- -----------------------------------------------------
START TRANSACTION;
USE `TaxiService_in`;
INSERT INTO `TaxiService_in`.`order_taxi` (`id`, `passenger_number`, `destination_address`, `pickup_address`, `cost`, `create_time`, `account_id`, `language_id`, `latitude_destination`, `longitude_destination`, `latitude_pickup`, `longitude_pickup`) VALUES (1, 2, 'dest address1', 'pickup add1', 7, now(), 2, 1, 50.457374, 30.523421, 50.442374, 30.521421);
INSERT INTO `TaxiService_in`.`order_taxi` (`id`, `passenger_number`, `destination_address`, `pickup_address`, `cost`, `create_time`, `account_id`, `language_id`, `latitude_destination`, `longitude_destination`, `latitude_pickup`, `longitude_pickup`) VALUES (2, 3, 'dest address2', 'pickup add2', 23, now(), 3, 1, 50.447374, 31.528421, 50.947374, 30.528421);
INSERT INTO `TaxiService_in`.`order_taxi` (`id`, `passenger_number`, `destination_address`, `pickup_address`, `cost`, `create_time`, `account_id`, `language_id`, `latitude_destination`, `longitude_destination`, `latitude_pickup`, `longitude_pickup`) VALUES (3, 1, 'адреса висадки', 'адреса посадки', 180, now(), 3, 2, 50.447377, 34.528421, 50.147374, 30.528421);

COMMIT;


-- -----------------------------------------------------
-- Data for table `TaxiService_in`.`category_translation`
-- -----------------------------------------------------
START TRANSACTION;
USE `TaxiService_in`;
INSERT INTO `TaxiService_in`.`category_translation` (`id`, `description`, `cost_by_km`, `language_id`) VALUES (1, 'premium', 10, 1);
INSERT INTO `TaxiService_in`.`category_translation` (`id`, `description`, `cost_by_km`, `language_id`) VALUES (2, 'business', 6, 1);
INSERT INTO `TaxiService_in`.`category_translation` (`id`, `description`, `cost_by_km`, `language_id`) VALUES (3, 'econom', 2, 1);
INSERT INTO `TaxiService_in`.`category_translation` (`id`, `description`, `cost_by_km`, `language_id`) VALUES (4, 'преміум', 250, 2);
INSERT INTO `TaxiService_in`.`category_translation` (`id`, `description`, `cost_by_km`, `language_id`) VALUES (5, 'бізнес', 100, 2);
INSERT INTO `TaxiService_in`.`category_translation` (`id`, `description`, `cost_by_km`, `language_id`) VALUES (6, 'економ', 15, 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `TaxiService_in`.`category`
-- -----------------------------------------------------
START TRANSACTION;
USE `TaxiService_in`;
INSERT INTO `TaxiService_in`.`category` (`id`, `category_translation_id`, `car_id`) VALUES (1, 1, 1);
INSERT INTO `TaxiService_in`.`category` (`id`, `category_translation_id`, `car_id`) VALUES (2, 4, 1);
INSERT INTO `TaxiService_in`.`category` (`id`, `category_translation_id`, `car_id`) VALUES (3, 2, 4);
INSERT INTO `TaxiService_in`.`category` (`id`, `category_translation_id`, `car_id`) VALUES (4, 5, 4);
INSERT INTO `TaxiService_in`.`category` (`id`, `category_translation_id`, `car_id`) VALUES (5, 3, 5);
INSERT INTO `TaxiService_in`.`category` (`id`, `category_translation_id`, `car_id`) VALUES (6, 6, 5);

COMMIT;


-- -----------------------------------------------------
-- Data for table `TaxiService_in`.`order_has_car`
-- -----------------------------------------------------
START TRANSACTION;
USE `TaxiService_in`;
INSERT INTO `TaxiService_in`.`order_has_car` (`order_id`, `car_id`) VALUES (1, 1);
INSERT INTO `TaxiService_in`.`order_has_car` (`order_id`, `car_id`) VALUES (2, 2);

COMMIT;


CREATE TRIGGER `order_taxi_AFTER_INSERT` AFTER INSERT ON `order_taxi` FOR EACH ROW BEGIN
	UPDATE account SET account.loyalty_id =
    (select loyalty.id from loyalty where loyalty.language_id = NEW.language_id
	 and loyalty.price <=
		(SELECT SUM(order_taxi.cost) as cost_sum from order_taxi where order_taxi.account_id = NEW.account_id)
	 order by loyalty.price desc
	 LIMIT 1)

    where account.id = NEW.account_id;
END